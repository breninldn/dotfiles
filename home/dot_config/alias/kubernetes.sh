alias k=kubectl
alias kl="kubectl logs"
alias klf="kubectl logs --follow"
# describe
alias kd="kubectl describe"
alias kdp="kubectl describe pod"
alias kdc="kubectl describe cronjob"
# delete
alias kx="kubectl delete"
alias kxp="kubectl delete pod"
# get
alias kg="kubectl get"
alias kgp="kubectl get pods"
alias kge="kubectl get events"
# apply
alias ka="kubectl apply"
