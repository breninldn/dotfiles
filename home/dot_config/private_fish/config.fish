if status is-interactive
    # Commands to run in interactive sessions can go here

    set fish_greeting # Disable welcome message

    ##### Aliases
    for f in $HOME/.config/alias/*.sh
      bass source "$f"
    end

    ##### Fish key bindings
    fish_hybrid_key_bindings
    # If no mode given it is normal mode
    bind -M default \ee edit_command_buffer
    bind -M insert \ee edit_command_buffer
    bind -M insert \c_ pager-toggle-search

    # fzf configuration
    set --export FZF_DEFAULT_OPTS \
      --ansi \
      --bind ctrl-u:preview-page-up,ctrl-d:preview-page-down,shift-tab:up,tab:down,ctrl-j:toggle+down,ctrl-k:toggle+up,ctrl-l:deselect-all,ctrl-space:toggle \
      --border \
      --cycle \
      --height=100% \
      --layout=reverse \
      --marker="*" \
      --multi \
      --preview-window=bottom:wrap \

    set --export fzf_history_opts \
        --preview-window=bottom:40%:wrap

    set --export fzf_fd_opts \
        --exclude .cache \
        --exclude .cargo \
        --exclude .config \
        --exclude .dbus \
        --exclude .fzf \
        --exclude .gnupg \
        --exclude .local \
        --exclude .pyenv \
        --exclude .nvm \
        --exclude go \
        --exclude .npm \
        --exclude .gitignore-boilerplates \
        --follow \
        --type file \


    #fzf plugin key bindings
    # \e5 is ctrl+/
    fzf_configure_bindings \
      --history=\cr \
      --variables=\ev \
      --directory=\eo \
      --git_log=\eg \
      --git_status=\es \
      --processes=\ep

    ###### Starship
    starship init fish | source

    ###### pyenv
    pyenv init - | source
    pyenv virtualenv-init - | source

    ###### direnv
    direnv hook fish | source

end
