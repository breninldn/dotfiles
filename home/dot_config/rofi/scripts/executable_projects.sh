#!/usr/bin/env -S bash --noprofile --norc

launch_project() {
	# Load direnv file and launch the project
	cd "$1" && direnv exec . neovide --nofork >/dev/null 2>&1
	# TODO: use --wezterm title flag when available
	# direnv exec "$1" wezterm start --cwd "$1" \
	# 	-- fish -ic "$EDITOR" >/dev/null 2>&1

}

# If rofi selection made
if [ $# -ne 0 ]; then
	working_dir="$ROFI_INFO"
	base="${working_dir##*/}"
	dir="${working_dir%/*}"
	coproc (launch_project "$working_dir" "${dir##*/}/$base")
	exit 0
fi

echo -en "\0no-custom\x1ftrue\n"

# List projects in rofi
for f in $(ls -d ~/projects/*/* ~/.local/share/chezmoi ~/.local/share/shelob); do
	base="${f##*/}"
	dir="${f%/*}"
	project_dir="${dir##*/}/$base"
	echo -en "$project_dir\0icon\x1faccessories-text-editor\x1finfo\x1f$f\x1fmeta\x1f$f\n"
done
