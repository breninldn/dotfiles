/**
 *
 * Author : Aditya Shakya (adi1090x)
 * Github : @adi1090x
 * 
 * Configuration For Rofi Version: 1.7.3

 * ----------
 *
 * Adapted for personal use
 **/

configuration {
	/*---------- General setting ----------*/
	modes: "combi,run,drun,window,calc,ssh,filebrowser,prj:~/.config/rofi/scripts/projects.sh";
	show-icons: true;
  sidebar-mode: false;
  hover-select: false;
	terminal: "rofi-sensible-terminal";
	sort: false;
	threads: 0;
	click-to-exit: true;
	case-sensitive: false;
	cycle: true;
	filter: "";
	scroll-method: 0;
	normalize-match: true;
	icon-theme: "Papirus";
	steal-focus: false;
	dpi: 0;
/*	ignored-prefixes: "";*/
/*	pid: "/run/user/1000/rofi.pid";*/
/*	cache-dir: ;*/

	/*---------- SSH settings ----------*/
	ssh-client: "ssh";
	ssh-command: "wezterm start -- ssh {host} [-p {port}]";
	parse-hosts: true;
	parse-known-hosts: false;

	/*---------- Drun settings ----------*/
	drun-categories: "";
	drun-match-fields: "name,generic,exec,categories,keywords";
	drun-display-format: "{name} [<span weight='light' size='small'><i>({generic})</i></span>]";
	drun-show-actions: false;
	drun-url-launcher: "xdg-open";
	drun-use-desktop-cache: false;
	drun-reload-desktop-cache: false;
	drun {
		/** Parse user desktop files. */
		parse-user:   true;
		/** Parse system desktop files. */
		parse-system: true;
    }

	/*---------- Run settings ----------*/
	run-command: "{cmd}";
	run-list-command: "";
	run-shell-command: "{terminal} -e {cmd}";

	/*---------- Fallback Icon ----------*/
	run,drun {
		fallback-icon: "application-x-addon";
	}

	/*---------- Window switcher settings ----------*/
	window-match-fields: "title,class,role,name,desktop";
	window-command: "wmctrl -i -R {window}";
	window-format: "{w} - {c} - {t:0}";
	window-thumbnail: false;

	/*---------- Combi settings ----------*/
	combi-modes: "window,drun,prj,ssh";
  /*	combi-hide-mode-prefix: false;*/
  combi-display-format: "{text}";
  combi {
    display-name: "Launch";
  }

	/*---------- Projects settings ----------*/
  prj {
    display-name: "Projects";
  }

	/*---------- History and Sorting ----------*/
	disable-history: false;
	sorting-method: "normal";
	max-history-size: 25;

  /*---------- Matching setting ----------*/
  matching: "fuzzy";
  tokenize: true;


	/*---------- Display setting ----------*/
	display-window: "Windows";
	display-windowcd: "Window CD";
	display-run: "Run";
	display-ssh: "SSH";
	display-drun: "Apps";
	display-combi: "Combi";
	display-keys: "Keys";
	display-filebrowser: "Files";

	/*---------- Other settings ----------*/
    timeout {
      action: "kb-cancel";
      delay:  0;
    }

	/*---------- Keybindings ----------*/
	kb-primary-paste: "Control+V,Shift+Insert";
	kb-secondary-paste: "Control+v,Insert";
	kb-clear-line: "";
	kb-move-front: "Control+a";
	kb-move-end: "Control+e";
	kb-move-word-back: "Alt+b,Control+Left";
	kb-move-word-forward: "Alt+f,Control+Right";
	kb-move-char-back: "Left,Control+b";
	kb-move-char-forward: "Right,Control+f";
	kb-remove-word-back: "Control+w,Control+BackSpace";
	kb-remove-word-forward: "Control+Alt+d";
	kb-remove-char-forward: "Delete";
	kb-remove-char-back: "BackSpace,Shift+BackSpace,Control+h";
	kb-remove-to-eol: "";
	kb-remove-to-sol: "";
	kb-accept-entry: "Control+m,Return,KP_Enter";
	kb-accept-custom: "Control+Return";
	kb-accept-custom-alt: "Control+Shift+Return";
	kb-accept-alt: "Shift+Return";
	kb-delete-entry: "Shift+Delete";
	kb-mode-next: "Alt+bracketright";
	kb-mode-previous: "Alt+bracketleft";
	kb-mode-complete: "";
	kb-row-left: "Control+Page_Up,Alt+h";
	kb-row-right: "Control+Page_Down,Alt+l";
	kb-row-up: "Up,Shift+Tab";
	kb-row-down: "Down";
	kb-page-prev: "Page_Up,Control+u";
	kb-page-next: "Page_Down,Control+d";
	kb-row-first: "Home,KP_Home";
	kb-row-last: "End,KP_End";
	kb-row-select: "Control+space";
	kb-screenshot: "Alt+S";
	kb-ellipsize: "Alt+period";
	kb-toggle-case-sensitivity: "grave,dead_grave";
	kb-toggle-sort: "Alt+grave";
	kb-cancel: "Escape,Control+g";
	kb-custom-1: "Alt+1";
	kb-custom-2: "Alt+2";
	kb-custom-3: "Alt+3";
	kb-custom-4: "Alt+4";
	kb-custom-5: "Alt+5";
	kb-custom-6: "Alt+6";
	kb-custom-7: "Alt+7";
	kb-custom-8: "Alt+8";
	kb-custom-9: "Alt+9";
	kb-custom-10: "Alt+0";
	kb-custom-11: "Alt+exclam";
	kb-custom-12: "Alt+at";
	kb-custom-13: "Alt+numbersign";
	kb-custom-14: "Alt+dollar";
	kb-custom-15: "Alt+percent";
	kb-custom-16: "Alt+dead_circumflex";
	kb-custom-17: "Alt+ampersand";
	kb-custom-18: "Alt+asterisk";
	kb-custom-19: "Alt+parenleft";
	kb-select-1: "Super+1";
	kb-select-2: "Super+2";
	kb-select-3: "Super+3";
	kb-select-4: "Super+4";
	kb-select-5: "Super+5";
	kb-select-6: "Super+6";
	kb-select-7: "Super+7";
	kb-select-8: "Super+8";
	kb-select-9: "Super+9";
	kb-select-10: "Super+0";
	ml-row-left: "ScrollLeft";
	ml-row-right: "ScrollRight";
	ml-row-up: "ScrollUp";
	ml-row-down: "ScrollDown";
	me-select-entry: "MousePrimary";
	me-accept-entry: "MouseDPrimary";
	me-accept-custom: "Control+MouseDPrimary";
}

@theme "custom"

// vim: ft=rasi
