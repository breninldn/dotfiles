#!/usr/bin/env bash
set -eo pipefail
python_version="3.11.3"
node_version="16.20.0"
venv="py3nvim"

source "$SHELOB_HOME/logger.sh" "SETUP NEOVIM"
source "$SHELOB_HOME/interactive.sh"

info "Setting up neovim"
info "Installing node for neovim via nvm"
if [[ $CHEZMOI_OS == "darwin" ]]; then
  source "/opt/homebrew/opt/nvm/nvm.sh"
else
  source "/usr/share/nvm/init-nvm.sh"
fi

nvm install "$node_version"
npm install -g neovim

info "Installing python for neovim via pyenv"
# Initialise pyenv
eval "$(pyenv init -)"
pyenv install --skip-existing "$python_version"
if [ ! -d "$HOME/.pyenv/versions/$venv" ]; then
  pyenv virtualenv "$python_version" "$venv" --system-site-packages
  pyenv activate "$venv"
  python -W ignore -m pip --disable-pip-version-check install pynvim PyYAML
fi
