#!/usr/bin/env -S bash --noprofile --norc
set -euo pipefail

source "$SHELOB_HOME/logger.sh" "FETCH SUMODULES"

git_command="git -C $CHEZMOI_WORKING_TREE"

if ! $git_command submodule status | grep --quiet '^-'; then
	warning "Submodules already initialised. Skipping!"
	exit 0
fi

info "Initialising projects & hosts submodules"

if [[ ! -e "$HOME/.ssh/id_ed25519" ]]; then
	info "Setting up GPG for fetching submodules"
	export GIT_SSH_COMMAND="ssh -F none -i /dev/null"
	export GPG_TTY="$(tty)"
	export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

	gpgconf --kill gpg-agent
	gpg-agent --daemon --steal-socket --enable-ssh-support --pinentry-program "$PINETRY_PROGRAM"
	gpg-connect-agent updatestartuptty /bye >/dev/null
else
	info "Using existing key to fetch submodules"
fi

# Pull private submodule for host and project configuration configurations
return=0
if ! $git_command -C "$CHEZMOI_WORKING_TREE" submodule update --init; then
	return=1
fi

gpgconf --kill gpg-agent
exit $return
