#!/usr/bin/env bash
#shellcheck disable=1091
#
# If a module name is passed to logger the logs are prefixed with
# module name

# Array of modules, module indexes are source files using the logger
declare -A shelob_logger_modules

__register_module() {
  shelob_logger_modules[${BASH_SOURCE[2]}]="$1"
}

if [[ -n "${1:-}" ]]; then
  __register_module "${1-}"
fi

# Return if logger is already sourced in the environment
[ -z "${SHELOB_LOGGER:-}" ] && SHELOB_LOGGER="Y" || return 0

source "${SHELOB_HOME}/colors.sh"

SHELOB_LOG_DEBUG="${SHELOB_LOG_DEBUG:-false}"

function __log_module() {
  local module=${shelob_logger_modules[${BASH_SOURCE[2]}]:-}
  if [[ -n "${module}" ]]; then
    printf "[%s]" "$module" | black white_bg bold
    printf " "
  fi
}

function __log_prefix() {
  local timestamp
  if [[ ${SHELOB_LOG_TIMESTAMP:-false} == true ]]; then
    printf -v timestamp '%(%Y-%m-%d %H:%M:%S)T'
    printf "%s - %s:" "$timestamp" "${1}"
  else
    printf "%s:" "${1}"
  fi
}

function __log_stdout() {
  printf " "
  printf "%s" "$@"
  printf '\n'
}

function __log_stderr() {
  printf " " >&2
  printf "%s" "$@" >&2
  printf '\n' >&2
}

function emergency() {
  __log_module >&2
  __log_prefix "emergency" | red_bg yellow bold underline >&2
  __log_stderr "$@"
  exit 1
}

function alert() {
  __log_module >&2
  __log_prefix "alert" | red_bg yellow >&2
  __log_stderr "$@"
}

function critical() {
  __log_module >&2
  __log_prefix "critical" | red underline bold >&2
  __log_stderr "$@"
}

function error() {
  __log_module >&2
  __log_prefix "error" | red bold >&2
  __log_stderr "$@"
}

function warning() {
  __log_module >&2
  __log_prefix "warning" | yellow bold >&2
  __log_stderr "$@"
}

function notice() {
  __log_module
  __log_prefix "notice" | blue bold
  __log_stdout "$@"
}

function info() {
  __log_module
  __log_prefix "info" | green
  __log_stdout "$@"
}

function debug() {
  if [[ ${SHELOB_LOG_DEBUG} == false ]]; then return; fi
  __log_module
  __log_prefix "debug" | cyan_bg black
  __log_stdout "$@"
}
