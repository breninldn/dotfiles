#!/usr/bin/env bash

# shellcheck disable=1117

[ -z "${SHELOB_TERM:-}" ] && SHELOB_TERM="Y" || return 0

# number of colors supported
__shelob_colors=$(tput colors 2>/dev/null) || __shelob_colors=0

#######################################
# Test if stdout is connected to tty.
# If this this command is piped to another command or is redirected to
# a file the test will fail returning 1 indicating stoud is not an interactive
# terminal.
#
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   0  : If stdout is connected to terminal
#   1  : If stdout is piped or redirected to a non-terminal output target
# Output:
#   None
#######################################
function terminal_connected() {
	[[ -t 1 ]]
}

#######################################
# Returns number of colors current terminal supports.
#
# Globals:
#    __shelob_colors (RO) : Number of colors terminal supports
# Arguments:
#   None
# Returns:
#   0
# Output:
#   Number of colours
#######################################
function number_of_colors() {
	echo "$__shelob_colors"
}

#######################################
# Test if terminal supports at least two colors
#
# Globals:
#  __shelob_colors (RO) : Number of colors terminal supports
# Arguments:
#   None
# Returns:
#   0  : If terminal supports at least 2 colors
#   1  : If terminal does not support colors or number of colors supported
#        are less then 2
# Output:
#   None
#######################################
function has_color2() {
	[[ -n $__shelob_colors ]] && [[ $__shelob_colors -ge 2 ]]
}

#######################################
# Test if terminal supports at least eight colors
#
# Globals:
#  __shelob_colors (RO) : Number of colors terminal supports
# Arguments:
#   None
# Returns:
#   0  : If terminal supports at least 8 colors
#   1  : If terminal does not support colors or number of colors supported
#        are less then 8
# Output:
#   None
#######################################
function has_color8() {
	[[ -n $__shelob_colors ]] && [[ $__shelob_colors -ge 8 ]]
}

#######################################
# Test if terminal supports at least sixteen colors
#
# Globals:
#  __shelob_colors (RO) : Number of colors terminal supports
# Arguments:
#   None
# Returns:
#   0  : If terminal supports at least 16 colors
#   1  : If terminal does not support colors or number of colors supported
#        are less then 16
# Output:
#   None
#######################################
function has_color16() {
	[[ -n $__shelob_colors ]] && [[ $__shelob_colors -ge 16 ]]
}

#######################################
# Test if function exists
#
# Globals:
#  None
# Arguments:
#  string ($2) : Function to test for existence
# Returns:
#   0  : If function exists for execution
#   1  : If function does not exist
# Output:
#   None
#######################################
function function_exists() {
	declare -F "${1:-}" >/dev/null
}

#######################################
# Print standard input to standard output
#
# Globals:
#  None
# Arguments:
#  None
# Returns:
# Output:
#  Standard input
#######################################
function print_stdin() {
	local first_line=true
	local line=
	# If last line does not end in new line, read does not count is a line
	# hence checking for not empty as well
	while IFS= read -r line || [ -n "$line" ]; do
		if [ "$first_line" == true ]; then
			first_line=false
		else
			printf "\n" # print newline before printing line if not first
		fi
		echo -n "$line"
	done
}
