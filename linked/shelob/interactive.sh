#!/usr/bin/env bash
#shellcheck disable=2229,1091
# Bash utility functions for interaction with user

[ -z "${SHELOB_INTERACTIVE:-}" ] && SHELOB_INTERACTIVE="Y" || return 0

source "${SHELOB_HOME}/colors.sh"
source "${SHELOB_HOME}/logger.sh"
source "${SHELOB_HOME}/string.sh"

function __get_default_answer() {
	local variable_name="$1"
	shift
	if [[ ${1:-} == "-d" || ${1:-} == "--default" ]]; then
		if [[ $# -ge 2 ]]; then
			read -r "$variable_name" <<<"$2"
			return 0
		else
			error "missing default value for option $1"
			exit 1
		fi
	fi
	return 1 # no default given
}

#######################################
# Usage: confirm [(-d|--default) <default-answer>] <question>
#
# Prompt user for a confirmation
# It uses default-answer( 'Y' 'y' 'N' 'n' ) if no answer has been received from stdin.
# If default-answer is not provided or invalid will keep asking
#
# Globals:
#    SHELOB_ANSWER_ALL (RO) : If true, answer yes and continue
#                           : If no default-answer provided exits with error 1
# Arguments:
#   default_answer  : Possible values: 'Y', 'y', 'N', 'n'
#   question        : The question to ask.
# Returns:
#   0                   : If user replied with either 'Y' or 'y'.
#   1                   : If user replied with either 'N' or 'n'.
# Output:
#   Print the question to ask.
#######################################
function confirm() {
	local default_answer=
	__get_default_answer default_answer "$@" && shift 2
	local question="${1:-} (y/n)"
	local yes="Y y"
	local no="N n"

	while :; do
		prompt_required --default "$default_answer" __answer "$question"
		# shellcheck disable=SC2154
		if [[ $yes =~ $__answer ]] || [[ $no =~ $__answer ]]; then break; fi
	done
	[[ $yes =~ $__answer ]]
}

#######################################
# Usage: prompt [(-d|--default) <default-input>] <variable-name> <message>
#
# Ask user to enter an input and wait to receive an input from stdin.
# Use default-input if no input received from stdin.
# If no defeault input is given empty response is accepted
#
# Globals:
#   SHELOB_ANSWER_ALL (RO) : If true, use default-input
# Arguments:
#   default_input     : Default input (with option -d or --default)
#   variable_name     : Variable to assign given input to
#   message           : Descriptive message to prompt user
# Returns:
#   0
#   1   : If variable name is not given
# Output:
#   None
#######################################
function prompt() {
	local default_input=
	__get_default_answer default_input "$@" && shift 2
	local variable_name=${1:-}
	local message=${2:-}

	if [[ -z "$variable_name" ]]; then
		error "Missing variable name"
		return 1
	fi

	if [[ -n "${default_input:-}" ]]; then
		message="${message} [${default_input}]"
	fi

	if [[ ${SHELOB_ANSWER_ALL:-} == true ]]; then
		debug "Answer all flag is set, using default input: $default_input"
		read -r "$variable_name" <<<"$default_input"
		return 0
	else
		printf "%s" "$message> " | blue bold
		read -r "$variable_name"
	fi
	debug "Response: ${!variable_name}"
	if [[ ${!variable_name} == "" ]]; then
		debug "Response is empty, using default input $default_input"
		read -r "$variable_name" <<<"$default_input"
	fi
}

#######################################
# Usage: prompt_required [(-d|--default) <default-input>] <variable-name> <message>
#
# Ask user to enter a required input and wait to receive an input from stdin.
# Use default-input if no input received from stdin.
# If no defeault input is given keeps asking for a response
#
# Globals:
#   SHELOB_ANSWER_ALL (RO) : If true, use default-input
#                          : If not default-input is provided exits with error 1
# Arguments:
#   default_input     : Default input (with option -d or --default)
#   variable_name     : Variable to assign given input to
#   message           : Descriptive message to prompt user
# Returns:
#   0
#   1   : If variable name is not given
# Output:
#   None
#######################################
function prompt_required() {
	local default_input=
	__get_default_answer default_input "$@" && shift 2
	local variable_name=${1:-}
	local message="${2}"
	if [[ -z "$default_input" ]]; then
		message="${2} (required)"
	fi
	while :; do
		prompt -d "$default_input" "$1" "$message"
		if [[ -n ${!variable_name} ]]; then break; fi
		if [[ ${SHELOB_ANSWER_ALL:-} == true ]]; then
			error "There is no default input, can not auto answer"
			return 1
		fi
	done
}

#######################################
# Usage: prompt_choice [(-d|--default) <option-number>] <variable-name> <message> <option>...
#
# Ask user to select an option and waits to receive a selection from stdin.
# Use option-number as default answer if no input received from stdin.
#
# Globals:
#   SHELOB_ANSWER_ALL (RO) : If true, use option-number as default selection
# Arguments:
#   option-number     : number of option in the list to use as default
#   variable-name     : Variable to assign given input to
#   message           : Descriptive prompt to display to user
#   options           : Array of options (option1 option2)
# Returns:
#   0
#   1   : If variable name is not given
#   1   : If message not provided
#   1   : If options not provided
# Output:
#   None
#######################################
function prompt_choice() {
	local default_input=
	__get_default_answer default_input "$@" && shift 2
	local variable_name=${1:-}
	if [[ -z $variable_name ]]; then
		error "Missing variable name"
		return 1
	fi
	local message=${2:-}
	if [[ -z $variable_name ]]; then
		error "Missing prompt"
		return 1
	fi
	shift 2

	debug "Number of options: ${#}"
	local selected_option=

	if [[ ${#} -eq 0 ]]; then
		error "Missing option arguments"
		return 1
	fi

	echo -e "$message\n" | blue bold
	local count=1
	for option in "${@}"; do
		echo -e "$count) $option\n" | blue bold
		count=$((count + 1))
	done

	while :; do
		prompt_required -d "$default_input" selected_option "Select an option"
		if [[ -z ${selected_option} ]]; then continue; fi

		if [[ $selected_option =~ ^[0-9]+$ ]] &&
			[[ $selected_option -le ${#} ]] &&
			[[ $selected_option -gt 0 ]]; then
			read -r "${variable_name}" <<<"${!selected_option}"
			break
		else
			debug "Clearing invalid selected option $selected_option"
			selected_option= # Clear invalid selection
		fi
	done
	debug "Selected answer: ${!variable_name}"
}
