local hs = hs
local Droppy = {}

Droppy.name = "Launcher"

--- modifiers
--- Variable
--- Modifier keys used when launching apps
---
--- Default value: `{"ctrl", "alt"}`
local modifiers = { "ctrl", "alt" }
local mapping = {}

function Droppy:new(config)
	setmetatable(config, {
		__index = {
			modifiers = modifiers,
			mapping = mapping,
		},
	})
	self.windows = {}

	local function launchApp(appName)
		local application = hs.application.open(appName, 1, true)
		local window = application:focusedWindow()
		if not window then
			os.execute("sleep 0.2")
			window = hs.window.focusedWindow()
		end
		self.windows[appName].window = window
	end

	local function getWindow(appName)
		local window = self.windows[appName].window
		if window and window:application() and window:application():isRunning() then
			return window
		end
	end

	local function isFocused(appName)
		return hs.window.focusedWindow() == getWindow(appName)
	end

	local function toggle(appName)
		local window = getWindow(appName)
		if not window then
			launchApp(appName)
			return
		end

		if isFocused(appName) then
			window:minimize()
		else
			print("Focusing", appName)
			window:focus()
			if not isFocused(appName) then
				launchApp(appName)
			end
		end
	end

	local function registerApp(app)
		local appName = app
		local opts = {}
		if type(app) == "table" then
			appName = app[1]
			opts = app
		end
		self.windows[appName] = { options = opts, window = nil }
		return appName
	end

	local function bind()
		for key, app in pairs(config.mapping) do
			local appName = registerApp(app)
			hs.hotkey.bind(config.modifiers, key, function()
				toggle(appName)
			end)
		end
	end

	bind()
end

return Droppy
