local hs = hs
local home = os.getenv("HOME")
-- Look for Spoons in ~/.config/hammerspoon as well
package.path = package.path .. ";" .. home .. "/.config/hammerspoon/?.spoon/init.lua"
package.path = package.path .. ";" .. home .. "/.hammerspoon/MySpoons/?.spoon/init.lua"

hs.window.animationDuration = 0
hs.hotkey.setLogLevel("warning")
hs.console.maxOutputHistory(1000)

-- Spoon repos
hs.loadSpoon("SpoonInstall")
-- spoon.SpoonInstall.repos = {
-- 	default = {
-- 		url = "https://github.com/Hammerspoon/Spoons",
-- 		desc = "Main Hammerspoon Spoon repository",
-- 		branch = "master",
-- 	},
-- }

-- Reload configuration
spoon.SpoonInstall:andUse("ReloadConfiguration", {
	start = true,
})

-- Use control as escape
hs.loadSpoon("ControlEscape")
spoon.ControlEscape:start()

-- Application Launchers
hs.loadSpoon("Droppy")
spoon.Droppy:new({
	mapping = {
		["return"] = "Wezterm",
		b = "Firefox",
		e = "Neovide",
		s = "Spotify",
		t = "Remember The Milk",
	},
	modifiers = { "cmd" },
})

-- Window manipulation
hs.hotkey.bind({ "cmd" }, "q", function()
	hs.window.focusedWindow():close()
end)

hs.hotkey.bind({ "cmd" }, "h", function()
	hs.window.focusedWindow():minimize()
end)

spoon.SpoonInstall:andUse("WinWin", {
	fn = function(WinWin)
		local move = function(key, option)
			hs.hotkey.bind({ "ctrl", "cmd" }, key, function()
				WinWin:moveAndResize(option)
			end)
		end
		local resize = function(key, option)
			hs.hotkey.bind({ "shift", "cmd" }, key, function()
				WinWin:stepResize(option)
			end)
		end
		move("m", "maximize")
		move("h", "halfleft")
		move("l", "halfright")
		move("j", "halfdown")
		move("k", "halfup")
		move("f", "fullscreen")
		resize("l", "right")
		resize("h", "left")
		resize("k", "up")
		resize("j", "down")
	end,
})

-- Command line launches

-- Keyboard help dialog
-- spoon.SpoonInstall:andUse("HSKeybindings", {
-- 	fn = function(HSKeybindings)
-- 		local visible = false
-- 		hs.hotkey.bind({ "cmd" }, "/", function()
-- 			if visible then
-- 				HSKeybindings:hide()
-- 				visible = false
-- 			else
-- 				HSKeybindings:show()
-- 				visible = true
-- 			end
-- 		end)
-- 	end,
-- })

-- Window switcher
-- local switcher = hs.window.switcher.new() -- default windowfilter: only visible windows, all Spaces
local switcher = hs.window.switcher.new(hs.window.filter.new():setCurrentSpace(true))
-- local switcher = hs.window.switcher.new(hs.window.filter.new():setCurrentSpace(true):setDefaultFilter({})) -- include minimized/hidden windows, current Space only
-- local switcher_browsers = hs.window.switcher.new({ "Safari", "Google Chrome" })

local function switchWindow(event)
	local flags = event:getFlags()
	local chars = event:getCharacters()
	if chars == "\t" and flags:containExactly({ "cmd" }) then
		switcher:next()
		return true
	elseif chars == string.char(25) and flags:containExactly({ "cmd", "shift" }) then
		switcher:previous()
		return true
	end
end
local tapCmdTab = hs.eventtap.new({ hs.eventtap.event.types.keyDown }, switchWindow)
tapCmdTab:start()

require("windows-mode")
