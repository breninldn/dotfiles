return {
	name = "mr install",
	builder = function()
		return {
			cmd = { "mr" },
			args = { "install" },
		}
	end,
	desc = "Install project dependencies",
	priority = 60,
	condition = {
		dir = "~/projects",
	},
}
