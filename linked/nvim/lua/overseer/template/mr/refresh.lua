return {
	name = "mr refresh",
	builder = function()
		return {
			cmd = { "mr" },
			args = { "refresh" },
		}
	end,
	desc = "Refresh project",
	priority = 30,
	condition = {
		dir = "~/projects",
	},
}
