return {
	name = "mr start",
	builder = function()
		return {
			cmd = { "mr" },
			args = { "start" },
		}
	end,
	desc = "Start/Run project",
	priority = 20,
	condition = {
		dir = "~/projects",
	},
}
