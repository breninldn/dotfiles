return {
	name = "mr cibuild",
	builder = function()
		return {
			cmd = { "mr" },
			args = { "cibuild" },
		}
	end,
	desc = "Run ci build",
	priority = 70,
	condition = {
		dir = "~/projects",
	},
}
