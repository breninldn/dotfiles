return {
	name = "mr shell",
	builder = function()
		return {
			cmd = { "mr" },
			args = { "shell" },
		}
	end,
	desc = "Open project shell",
	priority = 40,
	condition = {
		dir = "~/projects",
	},
}
