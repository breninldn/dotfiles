local keys = require("settings.keys.keys")

local state = {
  hidden = true,
  no_ignore = false, -- false to hide ignored
}

local function run(builtin_name, title, current_picker)
  local builtin = require("telescope.builtin")
  local action_state = require("telescope.actions.state")

  local toggle_hidden = function(prompt_bufnr)
    state.hidden = not state.hidden
    run(builtin_name, title, action_state.get_current_picker(prompt_bufnr))
  end

  local toggle_ignored = function(prompt_bufnr)
    state.no_ignore = not state.no_ignore
    run(builtin_name, title, action_state.get_current_picker(prompt_bufnr))
  end

  builtin[builtin_name]({
    cwd = current_picker and current_picker.cwd or vim.fn.getcwd(0),
    hidden = state.hidden,
    no_ignore = state.no_ignore,
    additional_args = {
      state.hidden and "--hidden" or "--no-hidden",
      state.no_ignore and "--no-ignore" or "--ignore",
    },
    default_text = current_picker and action_state.get_current_line() or "",
    prompt_title = title
      .. (state.hidden and " (incl. hidden)" or "")
      .. (state.no_ignore and " (incl. ignored)" or ""),
    attach_mappings = function(_, map)
      map({ "i", "n" }, keys.toggle.hidden, toggle_hidden)
      map({ "i", "n" }, keys.toggle.ignored, toggle_ignored)
      -- Map default mappings
      return true
    end,
  })
end

return {
  find_files = function()
    run("find_files", "Find Files")
  end,
  live_grep = function()
    run("live_grep", "Find Text")
  end,
  grep_string = function()
    local word_under_the_cursor = vim.fn.expand("<cword>")
    run("grep_string", "Find Word (" .. word_under_the_cursor .. ")")
  end,
}
