local M = {}
local mapper = require("utils.keymaps")

local function set_globals(globals)
  if not globals then
    return
  end

  for key, value in pairs(globals) do
    vim.g[key] = value
  end
end

local function set_options(options)
  if not options then
    return
  end

  local function set(key, value)
    vim.opt[key] = value
  end

  for key, value in pairs(options) do
    if not pcall(set, key, value) then
      vim.notify("Invalid option: " .. key, vim.log.levels.WARN)
    end
  end
end

local function set_keys(keys)
  if not keys then
    return
  end

  for _, key in pairs(keys) do
    if key.mods then
      mapper._map(key.mods, key[1], key[2], key.opts)
    else
      mapper.map(key[1], key[2], key.opts)
    end
  end
end

function M.set(settings)
  set_globals(settings.globals)
  set_options(settings.options)
  set_keys(settings.keys)
end

return M
