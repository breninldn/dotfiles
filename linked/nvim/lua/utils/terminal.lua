local M = {}

local terminals = {}

M.new = function(cwd)
  local Terminal = require("toggleterm.terminal").Terminal
  local pwd = cwd or vim.fn.getcwd(0)
  local term = terminals[pwd]
  if not term then
    term = Terminal:new({
      hidden = true,
      display_name = vim.fn.fnamemodify(vim.fn.getcwd(), ":h:t") .. "/" .. vim.fn.fnamemodify(vim.fn.getcwd(), ":t"),
      direction = "float",
      -- direction = "horizontal",
      -- direction = "vertical",
      dir = pwd,
      auto_scroll = false,
      float_opts = {
        border = "single",
        width = function()
          return math.floor(vim.o.columns * 0.98)
        end,
        height = function()
          return math.floor(vim.o.lines * 0.85)
        end,
        title_pos = "center",
      },
      on_creat = function(_)
        vim.cmd("startinsert!")
      end,
      -- on_open = function(_) end,
      -- on_close = function(_) end,
      on_exit = function(_)
        terminals[pwd] = nil
      end,
    })
    terminals[pwd] = term
  end
  term:toggle()
end

return M
