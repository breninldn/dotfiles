local M = {}
local icons = require("settings.icons")

local severity = vim.diagnostic.severity
M.ERROR = severity.ERROR
M.WARN = severity.WARN
M.INFO = severity.INFO
M.HINT = severity.HINT
M.ALL = { M.ERROR, M.WARN, M.INFO, M.HINT }

M.SIGNS = {
  [M.ERROR] = icons.diagnostics.ERROR,
  [M.WARN] = icons.diagnostics.WARN,
  [M.INFO] = icons.diagnostics.INFO,
  [M.HINT] = icons.diagnostics.HINT,
}

function M.prefix(diagnostic)
  severity = vim.diagnostic.severity[diagnostic.severity]
  return icons.diagnostics[severity], "Diagnostic" .. severity
end

return M
