local M = {}

M.run = function(cmd, args)
  local overseer = require("overseer")
  local task = overseer.new_task({
    cmd = cmd,
    args = args,
    strategy = {
      "toggleterm",
      on_create = function()
        vim.cmd("startinsert")
      end,
      quit_on_exit = "success",
    },
  })
  task:start()
end

return M
