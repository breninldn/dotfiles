local M = {}

function M.scaler(delta)
  return function()
    if delta == -1 then
      vim.g.neovide_scale_factor = vim.g.neovide_default_scale_factor
    else
      vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta
    end
  end
end

return M
