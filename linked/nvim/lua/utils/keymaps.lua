local M = {}

-- Functional wrapper for mapping custom keybindings
function M._map(m, lhs, rhs, opts)
  local options = { noremap = true, silent = true }
  if opts then
    options = vim.tbl_extend("force", options, opts)
  end
  vim.keymap.set(m, lhs, rhs, options)
end

-- Add all modes key mapping
function M.map(lhs, rhs, opts)
  M._map({ "n", "i", "v", "x", "t" }, lhs, rhs, opts)
end

-- Add all modes except terminal
function M.notmap(lhs, rhs, opts)
  M._map({ "n", "i", "v", "x" }, lhs, rhs, opts)
end

-- Add normal key mapping
function M.nmap(lhs, rhs, opts)
  M._map("n", lhs, rhs, opts)
end

-- Add insert key mapping
function M.imap(lhs, rhs, opts)
  M._map("i", lhs, rhs, opts)
end

-- Add visual mode key mapping
function M.vmap(lhs, rhs, opts)
  M._map("v", lhs, rhs, opts)
end

-- Add visual block mode key mapping
function M.xmap(lhs, rhs, opts)
  M._map("x", lhs, rhs, opts)
end

-- Add terminal mode key mapping
function M.tmap(lhs, rhs, opts)
  M._map("t", lhs, rhs, opts)
end

function M.resolve_menu(keymap)
  local function resolve(keys, leader)
    if type(keys) == "table" then
      local mapping = {}
      if keys.menu then
        leader = leader and leader .. keys.menu or keys.menu
        mapping = resolve(keys[1], leader)
        mapping.menu = leader
      else
        for name, rhs in pairs(keys) do
          mapping[name] = resolve(rhs, leader)
        end
      end
      return mapping
    else
      return leader and leader .. keys or keys
    end
  end

  return resolve(keymap)
end

return M
