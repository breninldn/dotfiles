local M = {}

local smart_splits = require("smart-splits")

M.go_left = smart_splits.move_cursor_left
M.go_right = smart_splits.move_cursor_right
M.go_up = smart_splits.move_cursor_up
M.go_down = smart_splits.move_cursor_down

M.push_left = smart_splits.swap_buf_left
M.push_right = smart_splits.swap_buf_right
M.push_up = smart_splits.swap_buf_up
M.push_down = smart_splits.swap_buf_down

M.resize_left = smart_splits.resize_left
M.resize_right = smart_splits.resize_right
M.resize_up = smart_splits.resize_up
M.resize_down = smart_splits.resize_down

M.take_left = function()
  M.push_left()
  M.go_left()
end

M.take_right = function()
  M.push_right()
  M.go_right()
end

M.take_up = function()
  M.push_up()
  M.go_up()
end

M.take_down = function()
  M.push_down()
  M.go_down()
end

return M
