return {
  {
    "alker0/chezmoi.vim",
    -- event = { { event = "BufRead", pattern = "$HOME/.local/share/chezmoi/**" } },
    lazy = false,
    init = function()
      vim.g["chezmoi#use_tmp_buffer"] = 1
      vim.g["chezmoi#source_dir_path"] = os.getenv("HOME") .. "/.local/share/chezmoi"
    end,
    config = function()
      require("plugins.chezmoi.autocommands")
    end,
  },
}
