-- [[ Chezmoi] ]

local vim = vim
local working_tree = vim.fn.expand("~") .. "/.local/share/chezmoi/"
local source_dir = working_tree .. "home/"
local scripts_dir = source_dir .. ".chezmoiscripts/"
local projects_dir = source_dir .. "projects/"
local private_dir = working_tree .. "private/"
local packages_dir = working_tree .. "packages/"
local tasks = require("utils.tasks")
-- local links_dir = working_tree .. "linked/"

local function is_source_path(path)
  return path:sub(1, #working_tree) == working_tree
end

local function is_managed(path)
  local path_style = is_source_path(path) and "source-absolute" or "relative"
  local cmd = "chezmoi managed -x scripts -p" .. path_style .. " | grep -x " .. path
  cmd = cmd .. " > /dev/null 2>&1"
  return os.execute(cmd) == 0
end

local function apply(path)
  local args = { "apply", path }
  if is_source_path(path) then
    table.insert(args, 2, "--source-path")
  end
  tasks.run("chezmoi", args)
end

local function autocmd(opts)
  vim.api.nvim_create_autocmd("BufWritePost", {
    pattern = opts.pattern,
    callback = function()
      -- If apply_path is not given, use the file being edited
      local paths = opts.apply_path or { vim.fn.expand("%:p") }
      if type(paths) == "string" then
        paths = { paths }
      end

      for _, apply_path in ipairs(paths) do
        if opts.filter and not opts.filter(apply_path) then
          return
        end
        apply(apply_path)
      end
    end,
  })
end

-- Apply chezmoi files automatically on save
-- Ignoring chezmoi scripts and files ignored by chezmoi
autocmd({
  pattern = source_dir .. "**",
  filter = is_managed,
})

autocmd({
  pattern = projects_dir .. "*/org/aws/config",
  apply_path = "~/.aws/config",
})

autocmd({
  pattern = projects_dir .. "*/org/ssh/config",
  apply_path = "~/.ssh/config",
})

autocmd({
  pattern = {
    source_dir .. "dot_config/crontab.tmpl",
    private_dir .. "*/crontab",
  },
  apply_path = {
    "~/.config/crontab",
    scripts_dir .. "run_once_after_setup-crontab.sh.tmpl",
  },
})

autocmd({
  pattern = packages_dir .. "linux/packages",
  apply_path = {
    scripts_dir .. "run_once_before_000-install-linux-packages.sh.tmpl",
  },
})

autocmd({
  pattern = packages_dir .. "darwin/Brewfile",
  apply_path = {
    scripts_dir .. "run_once_before_000-install-osx-packages.sh.tmpl",
  },
})

autocmd({
  pattern = packages_dir .. "pip.txt",
  apply_path = {
    scripts_dir .. "run_once_after_install-python-packages.sh.tmpl",
  },
})
