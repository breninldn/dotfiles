local vim = vim
return {
  -- Treesitter parsers
  {
    "nvim-treesitter/nvim-treesitter",
    ft = "lua",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "lua",
        "luadoc",
        "luap",
      })
    end,
  },
  -- LSP/Formatting/Linting binaries via Mason
  {
    "williamboman/mason.nvim",
    ft = "lua",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "stylua",
      })
    end,
  },
  -- Formatting configuration via conform
  {
    "stevearc/conform.nvim",
    ft = "lua",
    opts = {
      formatters_by_ft = {
        lua = { "stylua" },
      },
    },
  },
  -- LSP server configuration
  {
    "williamboman/mason-lspconfig.nvim",
    ft = "lua",
    opts = {
      servers = {
        lua_ls = {
          settings = {
            Lua = {
              workspace = {
                -- checkThirdParty = false,
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
              },
            },
          },
        },
      },
    },
  },
  -- {
  -- 	"nvim-neotest/neotest-python",
  -- 	config = function()
  -- 		-- get neotest namespace (api call creates or returns namespace)
  -- 		require("neotest").setup({
  -- 			adapters = {
  -- 				require("neotest-python")({
  -- 					-- Extra arguments for nvim-dap configuration
  -- 					-- See https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for values
  -- 					dap = { justMyCode = false },
  -- 					-- Command line arguments for runner
  -- 					-- Can also be a function to return dynamic values
  -- 					-- args = { "--log-level", "DEBUG" },
  -- 					-- Runner to use. Will use pytest if available by default.
  -- 					-- Can be a function to return dynamic value.
  -- 					runner = "unittest",
  -- 					-- Custom python path for the runner.
  -- 					-- Can be a string or a list of strings.
  -- 					-- Can also be a function to return dynamic value.
  -- 					-- If not provided, the path will be inferred by checking for
  -- 					-- virtual envs in the local directory and for Pipenev/Poetry configs
  -- 					-- python = ".venv/bin/python",
  -- 					-- Returns if a given file path is a test file.
  -- 					-- NB: This function is called a lot so don't perform any heavy tasks within it.
  -- 					-- is_test_file = function(file_path) end,
  -- 				}),
  -- 			},
  -- 		})
  -- 	end,
  -- },
}
