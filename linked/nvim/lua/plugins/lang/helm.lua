local vim = vim
return {
  -- LSP/Formatting/Linting binaries via Mason
  {
    "williamboman/mason.nvim",
    ft = { "templates/*", "helm" },
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "helm-ls",
      })
    end,
  },
  -- LSP server configuration
  {
    "williamboman/mason-lspconfig.nvim",
    ft = { "templates/*", "helm" },
    opts = {
      servers = {
        helm_ls = {
          yamlls = {
            path = "yaml-language-server",
          },
          capabilities = {
            offsetEncoding = "utf-8",
          },
          settings = {},
        },
      },
    },
  },
  {
    "towolf/vim-helm",
    ft = { "templates/*", "helm" },
  },
}
