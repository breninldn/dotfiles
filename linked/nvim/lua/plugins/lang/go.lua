-- Go shortcuts with which_key
vim.api.nvim_create_autocmd("FileType", {
  pattern = "go",
  callback = function()
    local buf = vim.api.nvim_get_current_buf()
    require("which-key").add({
      { "<localleader>", buffer = buf, group = "Go", nowait = true, remap = false },
      {
        "<localleader>G",
        "<cmd>GoGenerate %:p:h<Cr>",
        buffer = buf,
        desc = "Go Generate in Buffer Dir",
        nowait = true,
        remap = false,
      },
      { "<localleader>b", "<cmd>GoBuild<cr>", buffer = buf, desc = "Build", nowait = true, remap = false },
      {
        "<localleader>g",
        "<cmd>GoGenerate %<cr>",
        buffer = buf,
        desc = "Go Generate File",
        nowait = true,
        remap = false,
      },
      {
        "<localleader>i",
        "<cmd>GoInstallBinaries<cr>",
        buffer = buf,
        desc = "Install Go Dependencies",
        nowait = true,
        remap = false,
      },
      { "<localleader>r", "<cmd>GoRun<cr>", buffer = buf, desc = "Run", nowait = true, remap = false },
    })
  end,
})

return {
  -- Treesitter parsers
  {
    "nvim-treesitter/nvim-treesitter",
    ft = "go",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "go",
      })
    end,
  },
  -- LSP/Formatting/Linting binaries via Mason
  {
    "williamboman/mason.nvim",
    ft = "go",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "golangci-lint",
        "goimports",
      })
    end,
  },
  -- Formatting configuration via conform
  {
    "stevearc/conform.nvim",
    ft = "go",
    opts = {
      formatters_by_ft = {
        go = { "goimports" },
      },
    },
  },
  -- Linting configuration via nvim-lint
  {
    "mfussenegger/nvim-lint",
    ft = "go",
    opts = {
      linters_by_ft = {
        go = { "golangcilint" },
      },
    },
  },
  -- LSP server configuration
  {
    "williamboman/mason-lspconfig.nvim",
    ft = "go",
    opts = {
      servers = {
        gopls = {},
      },
    },
  },
  {
    "fatih/vim-go",
    ft = "go",
    config = function()
      -- Disable various vimgo settings ( rely on nvim lsp )
      vim.cmd("let g:go_code_completion_enabled = 0")
      vim.cmd("let g:go_fmt_autosave = 0")
      vim.cmd("let g:go_imports_autosave = 0")
      vim.cmd("let g:go_doc_keywordprg_enabled = 0")
      vim.cmd("let g:go_def_mapping_enabled = 0")
      vim.cmd("let g:go_gopls_enabled = 0")

      --timeout
      vim.cmd("let g:go_test_timeout = '60s'")
    end,
  },
  -- {
  --   "nvim-neotest/neotest-go",
  --   after = "neotest",
  --   config = function()
  --     -- get neotest namespace (api call creates or returns namespace)
  --     local neotest_ns = vim.api.nvim_create_namespace("neotest")
  --     vim.diagnostic.config({
  --       virtual_text = {
  --         format = function(diagnostic)
  --           local message =
  --           diagnostic.message
  --               :gsub("\n", " ")
  --               :gsub("\t", " ")
  --               :gsub("%s+", " ")
  --               :gsub("^%s+", "")
  --           return message
  --         end,
  --       },
  --     }, neotest_ns)
  --     require "neotest".setup({
  --       diagnostic = {
  --         enabled = true,
  --       },
  --       discovery = {
  --         enabled = false,
  --       },
  --       status = {
  --         virtual_text = true,
  --         signs = true,
  --       },
  --       adapters = {
  --         require "neotest-go" ({
  --           experimental = {
  --             test_table = true,
  --           },
  --           args = { "-count=1", "-timeout=60s", "-race", "-cover" },
  --         })
  --       }
  --     })
  --   end
  -- },
  -- {
  --   "leoluz/nvim-dap-go",
  --   config = function()
  --     require "dap-go".setup()
  --   end
  -- },
  -- {
  --   "olexsmir/gopher.nvim",
  --   ft = "go",
  --   config = function()
  --     require "gopher".setup {
  --       commands = {
  --         go = "go",
  --         gomodifytags = "gomodifytags",
  --         gotests = "gotests",
  --         impl = "impl",
  --         iferr = "iferr",
  --       },
  --     }
  --   end,
  --   requires = {
  --     "nvim-lua/plenary.nvim",
  --     "nvim-treesitter/nvim-treesitter",
  --   },
  -- }
}
