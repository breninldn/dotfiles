return {
  "neovim/nvim-lspconfig",
  version = "v1.x.x",
  config = function()
    require("lspconfig.ui.windows").default_options.border = require("settings.ui").border
  end,
}
