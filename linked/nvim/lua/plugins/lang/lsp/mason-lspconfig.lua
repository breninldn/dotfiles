return {
  "williamboman/mason-lspconfig.nvim",
  event = { "BufReadPre", "BufNewFile" },
  dependencies = {
    "williamboman/mason.nvim",
    "hrsh7th/cmp-nvim-lsp",
    "lukas-reineke/lsp-format.nvim",
    "neovim/nvim-lspconfig",
    "folke/neoconf.nvim",
  },
  opts = {
    autoformat = require("settings.lsp").autoformat,
    -- add any global capabilities here
    capabilities = {},
    ensure_installed = {},
    automatic_installation = true,
    servers = {
      -- efm = {
      --   on_attach = require("lsp-format").on_attach,
      --   init_options = { documentFormatting = true },
      -- },
    },
  },
  config = function(_, opts)
    -- Extend ensure installed list with opts.server list
    -- to automatically installed
    for server_name, _ in pairs(opts.servers) do
      vim.list_extend(opts.ensure_installed, { server_name })
    end

    local capabilities = vim.tbl_deep_extend(
      "force",
      -- {},
      vim.lsp.protocol.make_client_capabilities(),
      require("cmp_nvim_lsp").default_capabilities(),
      opts.capabilities or {}
    )

    require("mason-lspconfig").setup(opts)
    require("mason-lspconfig").setup_handlers({
      function(server_name) -- default handler
        local options = vim.tbl_deep_extend("force", {
          capabilities = vim.deepcopy(capabilities),
        }, opts.servers[server_name] or {})
        require("lspconfig")[server_name].setup(options)
      end,
    })

    -- local inlay_hint = vim.lsp.buf.inlay_hint or vim.lsp.inlay_hint
    -- if opts.inlay_hints.enabled and inlay_hint then
    --   Util.on_attach(function(client, buffer)
    --     if client.server_capabilities.inlayHintProvider then
    --       inlay_hint(buffer, true)
    --     end
    --   end)
    -- end
  end,
}
