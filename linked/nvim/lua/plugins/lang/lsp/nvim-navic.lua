return {
  "SmiteshP/nvim-navic",
  dependencies = { "neovim/nvim-lspconfig" },
  config = function()
    require("nvim-navic").setup({
      highlight = true,
      icons = require("settings.icons").kinds,
      lsp = { auto_attach = true },
    })
  end,
}
