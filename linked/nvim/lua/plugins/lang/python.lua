local vim = vim
return {
  -- Treesitter parsers
  {
    "nvim-treesitter/nvim-treesitter",
    ft = "python",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "python",
        "ninja",
        "rst",
        "toml",
      })
    end,
  },
  -- Formatting/Linting binaries via Mason
  {
    "williamboman/mason.nvim",
    ft = "python",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "mypy",
        -- "pylint",
        "isort",
        "black",
        "flake8",
        -- "ruff",
      })
    end,
  },
  -- Formatting configuration via conform
  {
    "stevearc/conform.nvim",
    ft = "python",
    opts = {
      formatters_by_ft = {
        python = { "isort", "black" },
      },
    },
  },
  -- Linting configuration via nvim-lint
  {
    "mfussenegger/nvim-lint",
    ft = "python",
    opts = {
      linters_by_ft = {
        python = { "flake8", "mypy" },
      },
    },
  },
  -- LSP server configuration
  {
    "williamboman/mason-lspconfig.nvim",
    ft = "python",
    opts = {
      servers = {
        pyright = {
          settings = {
            python = {
              analysis = {
                autoSearchPaths = true,
                diagnosticMode = "workspace",
                useLibraryCodeForTypes = true,
              },
            },
          },
        },
      },
    },
  },
  {
    "nvim-neotest/neotest",
    ft = "python",
    dependencies = {
      "nvim-neotest/neotest-python",
    },
    opts = function(_, opts)
      vim.list_extend(opts.adapters, {
        require("neotest-python")({
          -- Extra arguments for nvim-dap configuration
          -- See https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for values
          -- dap = { justMyCode = false },
          -- Command line arguments for runner
          -- Can also be a function to return dynamic values
          args = { "--log-level", "DEBUG" },
          -- Runner to use. Will use pytest if available by default.
          -- Can be a function to return dynamic value.
          runner = "pytest",
          -- Custom python path for the runner.
          -- Can be a string or a list of strings.
          -- Can also be a function to return dynamic value.
          -- If not provided, the path will be inferred by checking for
          -- virtual envs in the local directory and for Pipenev/Poetry configs
          -- python = ".venv/bin/python",
          -- Returns if a given file path is a test file.
          -- NB: This function is called a lot so don't perform any heavy tasks within it.
          -- is_test_file = function(file_path)
          --   ...
          -- end,
          -- !!EXPERIMENTAL!! Enable shelling out to `pytest` to discover test
          -- instances for files containing a parametrize mark (default: false)
          pytest_discover_instances = true,
        }),
      })
    end,
  },
}
