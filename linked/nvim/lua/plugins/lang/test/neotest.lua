return {
  "nvim-neotest/neotest",
  version = "5.x.x",
  opts = {
    adapters = {},
    consumers = {},
    overseer = {
      enabled = true,
      -- When this is true (the default), it will replace all neotest.run.*
      -- commands
      force_default = true,
    },
  },
  config = function(_, opts)
    opts.consumers.overseer = require("neotest.consumers.overseer")
    require("neotest").setup(opts)
  end,
  dependencies = {
    "stevearc/overseer.nvim",
    "folke/lazydev.nvim",
    "nvim-neotest/nvim-nio",
    "nvim-lua/plenary.nvim",
    "antoinemadec/FixCursorHold.nvim",
    "nvim-treesitter/nvim-treesitter",
  },
}
