return {
  "nvim-treesitter/nvim-treesitter",
  version = false, -- last release is way too old and doesn't work on Windows
  build = ":TSUpdate",
  event = { "BufReadPost", "BufNewFile" },
  dependencies = {
    {
      "nvim-treesitter/nvim-treesitter-textobjects",
      "nvim-treesitter/playground",
      "HiPhish/rainbow-delimiters.nvim",
      init = function()
        -- disable rtp plugin, as we only need its queries for mini.ai
        -- In case other textobject modules are enabled, we will load them
        -- once nvim-treesitter is loaded
        require("lazy.core.loader").disable_rtp_plugin("nvim-treesitter-textobjects")
        vim.g.x_load_textobjects = true
      end,
    },
  },
  cmd = { "TSUpdateSync" },
  keys = {
    { "<c-space>", desc = "Increment selection" },
    { "<bs>", desc = "Decrement selection", mode = "x" },
  },
  opts = function()
    local keys = require("settings.keys.keys")
    return {
      highlight = { enable = true },
      indent = { disable = { "python" } },
      ensure_installed = {
        "html",
        "ini",
        "javascript",
        "norg",
        "sql",
        "vim",
        "vimdoc",
      },
      incremental_selection = {
        enable = true,
        keymaps = {
          init_selection = "<C-space>",
          node_incremental = "<C-space>",
          scope_incremental = false,
          node_decremental = "<bs>",
        },
      },
      textobjects = {
        select = {
          enable = true,
          lookahead = true,
          keymaps = {
            ["am"] = "@function.outer",
            ["im"] = "@function.inner",
            ["aa"] = "@parameter.outer",
            ["ia"] = "@parameter.inner",
            ["a]"] = "@class.outer",
            ["i]"] = "@class.inner",
            -- ["ao"] = { query = { "@block.outer", "@loop.outer", "@conditional.outer" } },
            -- ["io"] = { query = { "@block.inner", "@loop.inner", "@conditional.inner" } },
          },
          include_surrounding_whitespace = true,
        },
        move = {
          enable = true,
          set_jumps = true, -- whether to set jumps in the jumplist
          goto_next_start = {
            ["]m"] = "@function.outer",
            ["]a"] = "@parameter.inner",
            ["]]"] = { query = "@class.outer", desc = "Next class start" },
            --
            -- You can use regex matching (i.e. lua pattern) and/or pass a list in a "query" key to group multiple queires.
            ["]o"] = { query = { "@block.outer", "@loop.outer", "@conditional.outer" } },
            --
            -- You can pass a query group to use query from `queries/<lang>/<query_group>.scm file in your runtime path.
            -- Below example nvim-treesitter's `locals.scm` and `folds.scm`. They also provide highlights.scm and indent.scm.
            -- ["]s"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
            ["]z"] = { query = "@fold", query_group = "folds", desc = "Next fold" },
          },
          goto_next_end = {
            ["]M"] = "@function.outer",
            ["]["] = "@class.outer",
            ["]O"] = { query = { "@block.outer", "@loop.outer", "@conditional.outer" } },
          },
          goto_previous_start = {
            ["[m"] = "@function.outer",
            ["[a"] = "@parameter.inner",
            ["[["] = "@class.outer",
            ["[o"] = { query = { "@block.outer", "@loop.outer", "@conditional.outer" } },
          },
          goto_previous_end = {
            ["[M"] = "@function.outer",
            ["[]"] = "@class.outer",
            ["[O"] = { query = { "@block.outer", "@loop.outer", "@conditional.outer" } },
          },
          -- Below will go to either the start or the end, whichever is closer.
          -- Use if you want more granular movements
          -- Make it even more gradual by adding multiple queries and regex.
          -- goto_next = {
          --   ["]d"] = "@conditional.outer",
          -- },
          -- goto_previous = {
          --   ["[d"] = "@conditional.outer",
          -- },
        },
        swap = {
          enable = true,
          swap_next = {
            [keys.edit.swap.parameter.next] = "@parameter.inner",
            [keys.edit.swap.method.next] = "@function.outer",
          },
          swap_previous = {
            [keys.edit.swap.parameter.previous] = "@parameter.inner",
            [keys.edit.swap.method.previous] = "@function.outer",
          },
        },
      },
    }
  end,
  config = function(_, opts)
    if type(opts.ensure_installed) == "table" then
      local added = {}
      opts.ensure_installed = vim.tbl_filter(function(lang)
        if added[lang] then
          return false
        end
        added[lang] = true
        return true
      end, opts.ensure_installed)
    end
    require("nvim-treesitter.configs").setup(opts)

    if vim.g.x_load_textobjects then
      if opts.textobjects then
        for _, mod in ipairs({ "move", "select", "swap", "lsp_interop" }) do
          if opts.textobjects[mod] and opts.textobjects[mod].enable then
            local Loader = require("lazy.core.loader")
            Loader.disabled_rtp_plugins["nvim-treesitter-textobjects"] = nil
            local plugin = require("lazy.core.config").plugins["nvim-treesitter-textobjects"]
            require("lazy.core.loader").source_runtime(plugin.dir, "plugin")
            break
          end
        end
      end
    end
  end,
}
