local vim = vim
return {
  -- Treesitter parsers
  {
    "nvim-treesitter/nvim-treesitter",
    ft="yaml",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "yaml",
      })
    end,
  },
  -- LSP/Formatting/Linting binaries via Mason
  {
    "williamboman/mason.nvim",
    ft="yaml",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "actionlint",
      })
    end,
  },
  -- Linting configuration via nvim-lint
  {
    "mfussenegger/nvim-lint",
    ft="yaml",
    opts = {
      linters_by_ft = {
        yaml = {"actionlint"}
      }
    },
  },
  -- LSP server configuration
  {
    "williamboman/mason-lspconfig.nvim",
    opts = {
      servers = {
        yamlls = {
          capabilities = {
            offsetEncoding = "utf-8",
          },
          settings = {},
        },
      },
    },
  },
}
