local vim = vim
return {
  -- Treesitter parsers
  {
    "nvim-treesitter/nvim-treesitter",
    ft = "terraform",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "terraform",
      })
    end,
  },
  -- Formatting configuration via conform
  {
    "stevearc/conform.nvim",
    ft = "terraform",
    opts = {
      formatters_by_ft = {
        terraform = { "terraform_fmt" },
      },
    },
  },
  -- LSP server configuration
  {
    "williamboman/mason-lspconfig.nvim",
    ft = "terraform",
    opts = {
      servers = {
        terraformls = {
          settings = {},
        },
        tflint = {
          settings = {},
        },
      },
    },
  },
}
