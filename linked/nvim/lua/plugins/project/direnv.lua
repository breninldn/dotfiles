return {
  "actionshrimp/direnv.nvim",
  lazy = false,
  opts = {
    type = "buffer", -- "buffer" | "dir"
    -- "buffer" direnv uses directory based on the current buffer. By default this is based around the 'FileType' autocmd.
    -- "dir" direnv uses directory based on vim's cwd (see this with `:pwd`). By default this is based around the 'DirChanged' autocmd.

    -- buffer_setup = ...
    -- allows you to control the type = 'buffer' setup's autocmd options.

    -- dir_setup = ...
    -- allows you to control the type = 'dir' setup's autocmd options.

    async = false, -- false | true
    -- if false, loading environment from direnv into vim is done synchronously. This will block the UI, so if the direnv setup takes a while, you may want to look into setting this to true.
    -- if true, vim will evaluate the direnv environment in the background, and then call the function passed as `opts.on_env_update` once evaluation is complete.

    -- on_hook_start = function() end,
    -- called just before executing direnv.

    -- on_env_update = function() end,
    -- called after direnv updates.

    -- on_no_direnv = function() end,
    -- called when no direnv is found for the current buffer.

    hook = {
      msg = "status", -- "status" | "diff" | nil
      -- message printed to the status line when direnv environment changes.
      -- - 'status' - shows the output of 'direnv status'.
      -- - 'diff' - shows the diff of environment variables.
      -- - nil - disables the message entirely.
    },
  },
}
