return {
  "nvim-telescope/telescope.nvim",
  version = "0.x.x",
  cmd = "Telescope",
  dependencies = { "nvim-lua/plenary.nvim", "folke/trouble.nvim" },
  config = function()
    local keys = require("settings.keys.keys")
    local toggle_and_move_down = function(...)
      local actions = require("telescope.actions")
      actions.toggle_selection(...)
      actions.move_selection_next(...)
    end

    local toggle_and_move_up = function(...)
      local actions = require("telescope.actions")
      actions.toggle_selection(...)
      actions.move_selection_previous(...)
    end

    local mappings = { -- applies to both normal and insert modes
      ["<C-t>"] = require("trouble.sources.telescope").open,
      ["<C-j>"] = toggle_and_move_down,
      ["<C-k>"] = toggle_and_move_up,
      [keys.list.toggle] = "toggle_selection",
      [keys.edit.clear] = "drop_all",
      [keys.list.next] = "move_selection_next",
      [keys.list.previous] = "move_selection_previous",
      [keys.window.split.horizontal] = "select_horizontal",
      [keys.window.split.vertical] = "select_vertical",
      [keys.prompt.next] = "cycle_history_next",
      [keys.prompt.previous] = "cycle_history_prev",
      [keys.scroll.alternate.down] = "preview_scrolling_down",
      [keys.scroll.alternate.up] = "preview_scrolling_up",
      [keys.scroll.down] = "results_scrolling_down",
      [keys.scroll.up] = "results_scrolling_up",
      [keys.window.quit] = "close",
      ["<C-v>"] = false,
      ["<C-x>"] = false,
    }

    require("telescope").setup({
      defaults = require("telescope.themes").get_dropdown({
        -- Default configuration for telescope goes here:
        prompt_prefix = " ",
        selection_caret = " ",
        dynamic_preview_title = true,
        -- setting theme to dropdown in config below, which uses center.
        -- layout_strategy = "center",
        layout_config = {
          -- layout strategcy configurations
          -- strategies: bottom_pane, center, cursor, horizontal, vertical
          bottom_pane = {
            height = 0.8,
            -- preview_cutoff = 120,
            prompt_position = "top",
          },
          center = {
            height = 0.3,
            mirror = true,
            anchor = "N",
            -- preview_cutoff = 40,
            prompt_position = "top",
            width = 0.9,
          },
          cursor = {
            height = 0.9,
            -- preview_cutoff = 40,
            width = 0.8,
          },
          horizontal = {
            height = 0.9,
            -- preview_cutoff = 120,
            prompt_position = "bottom",
            width = 0.9,
          },
          vertical = {
            height = 0.9,
            -- preview_cutoff = 40,
            prompt_position = "top",
            width = 0.9,
          },
        },
        mappings = {
          i = mappings,
          n = mappings,
        },
      }),
      pickers = {
        find_files = {
          find_command = { "fd", "--type", "file", "--type", "symlink" },
        },
      },
    })
  end,
}
