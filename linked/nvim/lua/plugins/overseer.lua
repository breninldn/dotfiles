return {
  "stevearc/overseer.nvim",
  config = function()
    local keys = require("settings.keys.keys")
    require("overseer").setup({
      task_list = {
        direction = "left",
      },
      strategy = {
        "toggleterm",
        auto_scroll = true,
        close_on_exit = false,
        quit_on_exit = "never",
        hidden = true,
        open_on_start = true,
        direction = "horizontal",
        on_create = function()
          vim.cmd("stopinsert")
        end,
      },
      templates = { "builtin" },
      task_editor = {
        -- Set keymap to false to remove default behavior
        bindings = {
          i = {
            [keys.list.select] = "NextOrSubmit",
            [keys.run] = "Submit",
            [keys.list.next] = "Next",
            [keys.list.previous] = "Prev",
            [keys.window.quit] = "Cancel",
          },
          n = {
            [keys.list.select] = "NextOrSubmit",
            [keys.run] = "Submit",
            [keys.list.next] = "Next",
            [keys.list.previous] = "Prev",
            [keys.window.quit] = "Cancel",
            [keys.help] = "ShowHelp",
          },
        },
      },
    })
  end,
  dependencies = { "stevearc/dressing.nvim" },
}
