return {
  "echasnovski/mini.surround",
  event = "VeryLazy",
  opts = {
    mappings = {
      add = "gs", -- Add surrounding in Normal and Visual modes
      delete = "ds", -- Delete surrounding
      find = "gs]", -- Find surrounding (to the right)
      find_left = "gs[", -- Find surrounding (to the left)
      highlight = "gsh", -- Highlight surrounding
      replace = "cs", -- Replace surrounding
      update_n_lines = "gsn", -- Update `n_lines`
    },
    -- Number of lines within which surrounding is searched
    n_lines = 55,
    search_method = "cover",
  },
}
