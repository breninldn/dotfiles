return {
  "echasnovski/mini.ai",
  event = { "BufReadPre", "BufNewFile" },
  version = "*",
  opts = {
    n_lines = 50,
    search_method = "cover_or_next",
  },
}
