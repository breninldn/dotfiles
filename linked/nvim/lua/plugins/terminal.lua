local vim = vim

return {
  {
    "akinsho/toggleterm.nvim",
    version = "*",
    opts = {
      -- size can be a number or function which is passed the current terminal
      size = function(term)
        if term.direction == "horizontal" then
          return vim.o.lines * 0.35
        elseif term.direction == "vertical" then
          return vim.o.columns * 0.35
        else
          return 20
        end
      end,
      hide_numbers = true,
      shade_terminals = true,
      -- shading_factor = -50, -- the percentage by which to lighten dark terminal background, default: -30
      -- shading_ratio = "<number>", -- the ratio of shading factor for light/dark terminal background, default: -3
    },
    config = true,
  },
  {
    "mrjones2014/smart-splits.nvim",
    lazy = false,
    opts = {},
  },
}
