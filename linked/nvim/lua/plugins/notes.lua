return {
  {
    "epwalsh/obsidian.nvim",
    version = "*", -- recommended, use latest release instead of latest commit
    lazy = true,
    ft = "markdown",
    cmd = {
      "ObsidianBacklinks",
      "ObsidianCheck",
      "ObsidianDailies",
      "ObsidianDebug",
      "ObsidianExtractNote",
      "ObsidianFollowLink",
      "ObsidianLink",
      "ObsidianLinks",
      "ObsidianLinkNew",
      "ObsidianNew",
      "ObsidianNewFromTemplate",
      "ObsidianOpen",
      "ObsidianPasteImg",
      "ObsidianQuickSwitch",
      "ObsidianRename",
      "ObsidianSearch",
      "ObsidianTags",
      "ObsidianToggleCheckbox",
      "ObsidianToday",
      "ObsidianTomorrow",
      "ObsidianTemplate",
      "ObsidianTOC",
      "ObsidianWorkspace",
      "ObsidianYesterday",
    },
    -- Replace the above line with this if you only want to load obsidian.nvim for markdown files in your vault:
    -- event = {
    --   -- If you want to use the home shortcut '~' here you need to call 'vim.fn.expand'.
    --   -- E.g. "BufReadPre " .. vim.fn.expand "~" .. "/my-vault/**.md"
    --   "BufReadPre path/to/my-vault/**.md",
    --   "BufNewFile path/to/my-vault/**.md",
    -- },
    dependencies = {
      -- Required.
      "nvim-lua/plenary.nvim",
    },
    opts = {
      workspaces = {
        {
          name = "personal",
          path = "~/Dropbox/Notes/personal",
        },
      },

      daily_notes = {
        -- Optional, if you keep daily notes in a separate directory.
        folder = "daily",
        -- Optional, if you want to change the date format for the ID of daily notes.
        -- date_format = "%Y-%m-%d",
        -- Optional, if you want to change the date format of the default alias of daily notes.
        -- alias_format = "%B %-d, %Y",
        -- Optional, if you want to automatically insert a template from your template directory like 'daily.md'
        -- template = nil,
      },
      templates = {
        subdir = "templates",
        -- date_format = "%Y-%m-%d-%a",
        -- time_format = "%H:%M",
      },

      -- Optional, customize how note IDs are generated given an optional title.
      note_id_func = function(title)
        -- Create note IDs in a Zettelkasten format with a timestamp and a suffix.
        -- In this case a note with the title 'My new note' will be given an ID that looks
        -- like '1657296016-my-new-note', and therefore the file name '1657296016-my-new-note.md'
        local suffix = ""
        if title ~= nil then
          -- If title is given, transform it into valid file name.
          suffix = title:gsub(" ", "-"):gsub("[^A-Za-z0-9-]", ""):lower()
        else
          -- If title is nil, just add 4 random uppercase letters to the suffix.
          for _ = 1, 4 do
            suffix = suffix .. string.char(math.random(65, 90))
          end
        end
        return tostring(os.time()) .. "-" .. suffix
      end,

      -- Optional, customize how note file names are generated given the ID, target directory, and title.
      note_path_func = function(spec)
        -- This is equivalent to the default behavior.
        local path = spec.dir / tostring(spec.id)
        return path:with_suffix(".md")
      end,

      -- Optional, customize how wiki links are formatted. You can set this to one of:
      --  * "use_alias_only", e.g. '[[Foo Bar]]'
      --  * "prepend_note_id", e.g. '[[foo-bar|Foo Bar]]'
      --  * "prepend_note_path", e.g. '[[foo-bar.md|Foo Bar]]'
      --  * "use_path_only", e.g. '[[foo-bar.md]]'
      -- Or you can set it to a function that takes a table of options and returns a string, like this:
      wiki_link_func = function(opts)
        return require("obsidian.util").wiki_link_id_prefix(opts)
      end,

      -- Optional, customize how markdown links are formatted.
      markdown_link_func = function(opts)
        return require("obsidian.util").markdown_link(opts)
      end,

      -- Optional, alternatively you can customize the frontmatter data.
      note_frontmatter_func = function(note)
        -- Add the title of the note as an alias.
        if note.title then
          note:add_alias(note.title)
        end

        local out = { id = note.id, date = os.date("%c"), aliases = note.aliases, tags = note.tags }

        -- `note.metadata` contains any manually added fields in the frontmatter.
        -- So here we just make sure those fields are kept in the frontmatter.
        if note.metadata ~= nil and not vim.tbl_isempty(note.metadata) then
          for k, v in pairs(note.metadata) do
            out[k] = v
          end
        end

        return out
      end,
    },
  },
  -- {
  --   "jghauser/papis.nvim",
  --   lazy = true,
  --   version = "v0.2.0", -- TODO: Update to latest papis after fix
  --   ft = "markdown",
  --   dependencies = {
  --     "kkharji/sqlite.lua",
  --     "MunifTanjim/nui.nvim",
  --     "pysan3/pathlib.nvim",
  --     "nvim-neotest/nvim-nio",
  --   },
  --   opts = {
  --
  --     -- List of enabled papis.nvim modules.
  --     enable_modules = {
  --       ["search"] = true, -- Enables/disables the search module
  --       ["completion"] = true, -- Enables/disables the completion module
  --       ["cursor-actions"] = true, -- Enables/disables the cursor-actions module
  --       ["formatter"] = true, -- Enables/disables the formatter module
  --       ["colors"] = true, -- Enables/disables default highlight groups (you
  --       -- probably want this)
  --       ["base"] = true, -- Enables/disables the base module (you definitely
  --       -- want this)
  --       ["debug"] = false, -- Enables/disables the debug module (useful to
  --       -- troubleshoot and diagnose issues)
  --     },
  --
  --     -- Defines citation formats for various filetypes. When the value is a table, then
  --     -- the first entry is used to insert citations, whereas the second will be used to
  --     -- find references (e.g. by the `cursor-action` module). `%s` stands for the reference.
  --     -- Note that the first entry is a string (where e.g. `\` needs to be excaped as `\\`)
  --     -- and the second a lua pattern (where magic characters need to be escaped with
  --     -- `%`; https://www.lua.org/pil/20.2.html).
  --     cite_formats = {
  --       tex = { "\\cite{%s}", "\\cite[tp]?%*?{%s}" },
  --       markdown = "#%s",
  --       rmd = "@%s",
  --       plain = "%s",
  --       org = { "[cite:@%s]", "%[cite:@%s]" },
  --       norg = "{= %s}",
  --     },
  --
  --     -- What citation format to use when none is defined for the current filetype.
  --     cite_formats_fallback = "plain",
  --
  --     -- Enable default keymaps.
  --     enable_keymaps = false,
  --
  --     -- Enable commands (disabling this still allows you to call the relevant lua
  --     -- functions directly)
  --     enable_commands = true,
  --
  --     -- Whether to enable the file system event watcher. When disabled, the database
  --     -- is only updated on startup.
  --     enable_fs_watcher = true,
  --
  --     -- The sqlite schema of the main `data` table. Only the "text" and "luatable"
  --     -- types are allowed.
  --     data_tbl_schema = {
  --       id = { "integer", pk = true },
  --       papis_id = { "text", required = true, unique = true },
  --       ref = { "text", required = true, unique = true },
  --       author = "text",
  --       editor = "text",
  --       year = "text",
  --       title = "text",
  --       type = "text",
  --       abstract = "text",
  --       time_added = "text",
  --       notes = "luatable",
  --       journal = "text",
  --       volume = "text",
  --       number = "text",
  --       author_list = "luatable",
  --       tags = "luatable",
  --       files = "luatable",
  --     },
  --
  --     -- Path to the papis.nvim database.
  --     db_path = vim.fn.stdpath("data") .. "/papis_db/papis-nvim.sqlite3",
  --
  --     -- Name of the `yq` executable.
  --     yq_bin = "yq",
  --
  --     -- Function to execute when adding a new note. `ref` is the citation key of the
  --     -- relevant entry and `notes_name` is defined in `papis_python` above.
  --     create_new_note_fn = function(papis_id, notes_name)
  --       local testing_session = require("papis.config")["enable_modules"]["testing"]
  --       local testing_conf_path = ""
  --       if testing_session then
  --         testing_conf_path = "-c ./tests/papis_config "
  --       end
  --       vim.fn.system(
  --         string.format(
  --           "papis " .. testing_conf_path .. "update --set notes %s papis_id:%s",
  --           vim.fn.shellescape(notes_name),
  --           vim.fn.shellescape(papis_id)
  --         )
  --       )
  --     end,
  --
  --     -- Filetypes that start papis.nvim.
  --     init_filetypes = { "markdown", "norg", "yaml" },
  --
  --     -- Configuration of the search module.
  --     ["search"] = {
  --
  --       -- Wether to enable line wrap in the telescope previewer.
  --       wrap = true,
  --
  --       -- What keys to search for matches.
  --       search_keys = { "author", "editor", "year", "title", "tags" },
  --
  --       -- The format for the previewer. Each line in the config represents a line in
  --       -- the preview. For each line, we define:
  --       --   1. The key whose value is shown
  --       --   2. How it is formatted (here, each is just given as is)
  --       --   3. The highlight group
  --       --   4. (Optionally), `show_key` causes the key's name to be displayed in addition
  --       --      to the value. When used, there are then another two items defining the
  --       --      formatting of the key and its highlight group. The key is shown *before*
  --       --      the value in the preview (even though it is defined after it in this
  --       --      configuration (e.g. `title = Critique of Pure Reason`)).
  --       -- `empty_line` is used to insert an empty line
  --       preview_format = {
  --         { "author", "%s", "PapisPreviewAuthor" },
  --         { "year", "%s", "PapisPreviewYear" },
  --         { "title", "%s", "PapisPreviewTitle" },
  --         { "empty_line" },
  --         { "ref", "%s", "PapisPreviewValue", "show_key", "%s = ", "PapisPreviewKey" },
  --         { "type", "%s", "PapisPreviewValue", "show_key", "%s = ", "PapisPreviewKey" },
  --         { "tags", "%s", "PapisPreviewValue", "show_key", "%s = ", "PapisPreviewKey" },
  --         { "files", "%s", "PapisPreviewValue", "show_key", "%s = ", "PapisPreviewKey" },
  --         { "notes", "%s", "PapisPreviewValue", "show_key", "%s = ", "PapisPreviewKey" },
  --         { "journal", "%s", "PapisPreviewValue", "show_key", "%s = ", "PapisPreviewKey" },
  --         { "abstract", "%s", "PapisPreviewValue", "show_key", "%s = ", "PapisPreviewKey" },
  --       },
  --
  --       -- The format of each line in the the results window. Here, everything is show on
  --       -- one line (otherwise equivalent to points 1-3 of `preview_format`).
  --       results_format = {
  --         { "author", "%s ", "PapisResultsAuthor" },
  --         { "year", "(%s) ", "PapisResultsYear" },
  --         { "title", "%s", "PapisResultsTitle" },
  --       },
  --     },
  --
  --     -- Configuration of the cursor-actions module.
  --     ["cursor-actions"] = {
  --
  --       -- The format of the popup shown on `:PapisShowPopup` (equivalent to points 1-3
  --       -- of `preview_format`)
  --       popup_format = {
  --         { "author", "%s", "PapisPopupAuthor" },
  --         { "year", "%s", "PapisPopupYear" },
  --         { "title", "%s", "PapisPopupTitle" },
  --       },
  --     },
  --
  --     -- Configuration of formatter module.
  --     ["formatter"] = {
  --
  --       -- This function runs when first opening a new note. The `entry` arg is a table
  --       -- containing all the information about the entry (see above `data_tbl_schema`).
  --       -- This example is meant to be used with the `markdown` filetype.
  --       format_notes_fn = function(entry)
  --         -- Some string formatting templates (see above `results_format` option for
  --         -- more details)
  --         local title_format = {
  --           { "author", "%s ", "" },
  --           { "year", "(%s) ", "" },
  --           { "title", "%s", "" },
  --         }
  --         -- Format the strings with information in the entry
  --         local title = require("papis.utils"):format_display_strings(entry, title_format, true)
  --         -- Grab only the strings (and disregard highlight groups)
  --         for k, v in ipairs(title) do
  --           title[k] = v[1]
  --         end
  --         -- Define all the lines to be inserted
  --         local lines = {
  --           "---",
  --           'title: "Notes -- ' .. table.concat(title) .. '"',
  --           "---",
  --           "",
  --         }
  --         -- Insert the lines
  --         vim.api.nvim_buf_set_lines(0, 0, #lines, false, lines)
  --         -- Move cursor to the bottom
  --         vim.cmd("normal G")
  --       end,
  --       -- This function runs when inserting a formatted reference (currently by `f/c-f` in
  --       -- Telescope). It works similarly to the `format_notes_fn` above.
  --       format_references_fn = function(entry)
  --         local reference_format = {
  --           { "ref", "#%s: ", "" },
  --           { "author", "%s ", "" },
  --           { "year", "(%s). ", "" },
  --           { "title", "*%s.* ", "" },
  --           { "journal", "%s. ", "" },
  --           { "volume", "%s", "" },
  --           { "number", "(%s)", "" },
  --         }
  --         local reference_data = require("papis.utils"):format_display_strings(entry, reference_format)
  --         for k, v in ipairs(reference_data) do
  --           reference_data[k] = v[1]
  --         end
  --         return table.concat(reference_data)
  --       end,
  --     },
  --
  --     -- Configurations relevant for parsing `info.yaml` files.
  --     ["papis-storage"] = {
  --
  --       -- As lua doesn't deal well with '-', we define conversions between the format
  --       -- in the `info.yaml` and the format in papis.nvim's internal database.
  --       key_name_conversions = {
  --         time_added = "time-added",
  --       },
  --
  --       -- The format used for tags. Will be determined automatically if left empty.
  --       -- Can be set to `tbl` (if a lua table), `,` (if comma-separated), `:` (if
  --       -- semi-colon separated), ` ` (if space separated).
  --       tag_format = nil,
  --
  --       -- The keys which `.yaml` files are expected to always define. Files that are
  --       -- missing these keys will cause an error message and will not be added to
  --       -- the database.
  --       required_keys = { "papis_id", "ref" },
  --     },
  --
  --     -- Configuration of logging.
  --     log = {
  --
  --       -- What levels to log. Debug mode is more conveniently
  --       -- enabled in `enable_modules`.
  --       level = "info",
  --     },
  --   },
  -- },
  {
    "jalvesaq/cmp-zotcite",
    ft = "markdown",
    dependencies = {
      "hrsh7th/nvim-cmp",
      "jalvesaq/zotcite",
      "nvim-treesitter/nvim-treesitter",
      "nvim-telescope/telescope.nvim",
    },
    config = function()
      require("cmp_zotcite").setup({
        filetypes = { "pandoc", "markdown", "rmd", "quarto" },
      })
      require("cmp").setup({
        sources = {
          { name = "cmp_zotcite" },
        },
      })
    end,
  },
  {
    "jalvesaq/zotcite",
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "nvim-telescope/telescope.nvim",
    },
    config = function()
      require("zotcite").setup({
        -- your options here (see doc/zotcite.txt)
      })
    end,
  },
}
