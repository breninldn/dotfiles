return {
  "nvim-lualine/lualine.nvim",
  event = { "BufReadPre", "BufNewFile" },
  dependencies = { "SmiteshP/nvim-navic" },
  opts = function()
    local icons = require("settings.icons")
    local navic = require("nvim-navic") ---@diagnostic disable-line

    return {
      options = {
        theme = "auto",
        globalstatus = false,
        disabled_filetypes = { statusline = { "dashboard", "alpha" } },
      },
      sections = {
        lualine_a = {
          "branch",
        },
        lualine_b = {
          {
            "diff",
            symbols = {
              added = icons.git.added,
              modified = icons.git.modified,
              removed = icons.git.removed,
            },
          },
        },
        lualine_c = {
          {
            "filename",
            path = 0,
            symbols = {
              modified = icons.file.modified,
              readonly = icons.file.readonly,
              unnamed = "",
            },
          },
        },
        lualine_x = {
          {
            function()
              return "  " .. require("dap").status()
            end,
            cond = function()
              return package.loaded["dap"] and require("dap").status() ~= ""
            end,
            -- color = Util.ui.fg("Debug"),
          },
          {
            require("lazy.status").updates,
            cond = require("lazy.status").has_updates,
          },
          { "encoding", separator = "", padding = { left = 0, right = 0 } },
          { "fileformat" },
          { "filetype", icon_only = false, padding = { left = 1, right = 1 } },
        },
        lualine_y = {
          "overseer",
          -- function()
          --   return " " .. os.date("%R")
          -- end,
        },
        lualine_z = {
          { "location", separator = " ", padding = { left = 0, right = 0 } },
          { "progress", separator = "", padding = { left = 0, right = 1 } },
        },
      },
      winbar = {
        lualine_a = {
          {
            "mode",
            fmt = function()
              return "󰀘"
            end,
          },
          function()
            return vim.fn.fnamemodify(vim.fn.getcwd(), ":h:t") .. "/" .. vim.fn.fnamemodify(vim.fn.getcwd(), ":t")
          end,
        },
        lualine_b = {},
        lualine_c = {
          {
            function()
              return vim.fn.expand("%")
            end,
          },
          {
            "diagnostics",
            symbols = {
              error = icons.diagnostics.ERROR,
              warn = icons.diagnostics.WARN,
              info = icons.diagnostics.INFO,
              hint = icons.diagnostics.HINT,
            },
          },
          { "navic", color_correction = nil },
        },
      },
      inactive_winbar = {
        lualine_c = {
          {
            function()
              return vim.fn.expand("%")
            end,
          },
        },
      },
      extensions = {
        "fugitive",
        "lazy",
        "man",
        "mason",
        "oil",
        "overseer",
        "symbols-outline",
        "toggleterm",
        "trouble",
      },
    }
  end,
}
