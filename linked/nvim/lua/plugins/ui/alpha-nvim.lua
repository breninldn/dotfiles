return {
  "goolord/alpha-nvim",
  event = "VimEnter",
  opts = function()
    return require("alpha.themes.startify").config
  end,
}
