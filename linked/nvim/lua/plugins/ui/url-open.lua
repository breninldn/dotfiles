return {
  "sontungexpt/url-open",
  event = "VeryLazy",
  cmd = "URLOpenUnderCursor",
  config = function()
    -- default values
    require("url-open").setup({
      highlight_url = {
        all_urls = {
          enabled = true,
        },
      },
      deep_pattern = true,
    })
  end,
}
