return {
  {
    "ciaranm/securemodelines",
    lazy = false,
    config = function()
        -- stylua: ignore
        vim.g.secure_modelines_allowed_items = {
          "expandtab", "et", "noexpandtab", "noet",
          "filetype", "ft",
          "foldmethod", "fdm",
          "readonly", "ro", "noreadonly", "noro",
          "rightleft", "rl", "norightleft", "norl",
          "shiftwidth", "sw",
          "softtabstop", "sts",
          "tabstop", "ts",
          "textwidth", "tw",
        }
      -- number of lines to check from beginning and end
      vim.g.secure_modelines_modelines = 5
      vim.g.secure_modelines_verbose = 1
    end,
  },
  { "tpope/vim-repeat", event = "VeryLazy" },
  {
    "echasnovski/mini.bufremove",
  },
  { "numtostr/BufOnly.nvim", cmd = { "BufOnly" } },
}
