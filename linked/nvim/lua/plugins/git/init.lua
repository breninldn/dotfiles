return {
  require("plugins.git.fugitive"),
  require("plugins.git.gitsigns"),
  require("plugins.git.gitlinker"),
  require("plugins.git.octo"),
}
