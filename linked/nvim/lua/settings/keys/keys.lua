return {
  -- Insert mappings
  insert = {
    -- Mimic Emacs Line Editing in Insert Mode
    char = { back = "<C-b>", forward = "<C-f>" },
    word = { back = "<M-b>", forward = "<M-f>", delete = "<C-BS>" },
    line = { start = "<C-a>", _end = "<C-e>" },
  },

  -- Terminal mappings
  term = {
    return_to_normal = "<ESC><ESC>", -- Double Escape to return to normal mode
  },

  -- Visual mappings
  visual = {},

  -- Visual block mappings
  visual_block = {},

  -- Normal mappings
  buffer = {
    close = "<M-x>",
    close_force = "<M-X>",
    write = "<M-w>",
    write_all = "<M-W>",
    undo_closed = "<M-u>",
  },
  complete = "<C-Space>",
  edit = {
    clear = "<C-l>", -- clear highlights ( except in terminal mode)
    line_break = "<NL>", -- break line in normal mode with C-j
    swap = {
      line = { next = "]e", previous = "[e" } ,
      parameter = { next = "gw]a", previous = "gw[a"},
      method = { next = "gw]m", previous = "gw[m"}
    }
  },
  git = {
    hunk = { next = "]g", previous = "[g" },
  },
  goto = {
    buffer = {
      first = "[B",
      last = "]B",
      next = "]b",
      previous = "[b"
    },
    char = "<M-f>",
    diagnostics = { next = "]d", previous = "[d" },
    line = "<M-a>"
  },
  help = "g?",
  list = { next = "<Tab>", previous = "<S-Tab>", select = "<CR>", toggle = "<C-Space>" },
  open = {
    buffers = "<M-b>",
    files = "<M-o>",
    projects = "<M-p>",
    terminal = { cwd = "<M-CR>", file_dir = "<M-S-CR>" },
  },
  prompt = { next = "<C-n>", previous = "<C-p>" },
  scroll = { down = "<C-d>", up = "<C-u>", alternate = { down = "<M-d>", up = "<M-u>" } },
  search = {
    cursor_word = "<M-8>",
    text = "<M-/>",
  },
  preview = "<C-v>",
  tab = { new = "<M-n>", next = "<M-]", previous = "<M-[" },
  toggle = { hidden = "<M-.>", ignored = "<M-i>" },
  window = {
    quit = "<M-q>",
    quit_all = "<M-Q>",
    fullscreen = "<M-C-F>",
    nav = {
      left = "<M-h>",
      right = "<M-l>",
      up = "<M-k>",
      down = "<M-j>",
    },
    move = {
      left = "<M-C-H>",
      right = "<M-C-L>",
      up = "<M-C-K>",
      down = "<M-C-J>",
    },
    push = {
      left = "<M-C-Left>",
      right = "<M-C-Right>",
      up = "<M-C-Up>",
      down = "<M-C-Down>",
    },
    resize = {
      left = "<M-H>",
      right = "<M-L>",
      up = "<M-K>",
      down = "<M-J>",
      equal = "<M-S-BS>",
    },
    split = {
      horizontal = "<M-;>",
      vertical = "<M-'>",
    },
  },
  run = "<C-CR>",
  tasks = {
    run = "<M-r>",
    show = "<M-t>",
  },
}
