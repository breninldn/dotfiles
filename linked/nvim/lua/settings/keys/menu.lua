return {
  leader = " ",
  localleader = "  ",

  close = {
    buffer = {
      menu = "<leader>x",
      {
        current = "x",
        others = "o",
      },
    },
    window = {
      menu = "<leader>q",
      {
        all = "a",
        others = "o",
      },
    },
  },

  debug = {
    menu = "<leader>d",
    {
      continue = "c",
      run_to_cursor = "C",
      toggle_ui = "U",
      step = { back = "[", into = "<CR>", out = "o", over = "]" },
      session = { disconnect = "d", get = "g", start = "s", quit = "q" },
      pause = "p",
      repl = "r",
      toggle_breakpoint = "t",
    },
  },

  edit = {
    menu = "<leader>e",
    {
      generate = {
        doc = {
          current = "e",
          class = "c",
          func = "f",
          file = "l",
          type = "t",
        },
      },
    },
  },

  file = {
    menu = "<leader>f",
    {
      delete = "d",
      delete_force = "D",
      move = "m",
      rename = "r",
    },
  },

  git = {
    menu = "<leader>g",
    {
      blame = { line = "m", buffer = "M" },
      browse = { file = "b", repo = "B" },
      copy = { file_url = "y", repo_url = "Y" },
      checkout = {
        menu = "c",
        {
          branch = "b",
          commit = { all = "c", buffer = "C" },
          stash = "s",
        },
      },
      diff = "d",
      edit = "e",
      hub = "h",
      log = { all = "l", buffer = "L" },
      mergetool = "D",
      push_pull = {
        menu = "p",
        {
          pull = "l",
          pull_force = "L",
          push = "s",
          push_force = "S",
        },
      },
      reset = { hunk = "r", buffer = "R" },
      stage = { hunk = "a", buffer = "A", undo = { hunk = "u", buffer = "U" } },
      view = { hunk = "g" },
      status = "s",
    },
  },

  lsp = {
    menu = "<leader>l",
    {
      code_action = "a",
      codelens = "c",
      diagnostics = { all = "D", buffer = "d", hover = "e" },
      format = "f",
      implementation = "i",
      logs = "g",
      outline = "o",
      packages = "p",
      references = "r",
      rename = "R",
      signature_help = "h",
      symbols = "s",
    },
  },

  notes = {
    menu = "<leader>n",
    {
      extract = "E",
      insert = { link = "i", template = "T" },
      new = { note = "c", note_from_template = "C", link = "L" },
      search = { backlinks = "b", links = "l", notes = "n", tags = "g" },
      today = "t",
      tomorrow = "m",
      yesterday = "y",
      workspace = "w",
    },
  },

  open = {
    menu = "<leader>o",
    {
      quickfix = "q",
      location_list = "l",
      recent_files = "r",
    },
  },

  search = {
    menu = "<leader>/",
    {
      color_scheme = "c",
      help = "h",
      man_page = "m",
      keys = "k",
      todo = "t",
    },
  },

  toggle = {
    menu = "<leader>t",
    {
      cursor = { line = "l", column = "c" },
      color_codes = "z",
      fold = "f",
      list_chars = "h",
      numbers = "n",
      numbers_relative = "r",
      spell = "s",
    },
  },

  project = {
    menu = "<leader>p",
    {
      add = "a",
      remove = "x",
    },
  },

  run = {
    menu = "<leader>r",
    {
      command = "r",
      test = { nearest = "t", buffer = "T", debug = "d", summary = "s" },
    },
  },

  status = {
    menu = "<leader>s",
    {
      mason = "m",
      conform = "f",
      plugins = "p",
      treesitter = "t",
      lsp = "l",
    },
  },
}
