local utl = require("utils.lsp")
local border = require("settings.ui").border

return {
  enabled = true, -- custom, see setup
  autoformat = true,
  hover = {
    title = "",
    border = border,
  },
  signature_help = {
    border = border,
  },
  diagnostics = {
    update_in_insert = false,
    severity_sort = true,
    float = { -- what to show in floating window popup
      border = border,
      -- Show diagnostics from the whole buffer (`buffer"`), the current cursor line (`line`), or the current cursor position (`cursor`).
      scope = "cursor",
      severity_sort = true,
      severity = utl.ALL,
      -- header = "",
      -- format = function() end
      source = "if_many", -- boolean|'if_many'
      -- prefix = require("settings.icons"),
      prefix = utl.prefix,
      -- suffix = ""
    },
    signs = {
      severity = utl.ALL,
      text = utl.SIGNS,
    },
    underline = {
      severity = { utl.ERROR, utl.WARN },
    },
    virtual_text = {
      source = "if_many",
      severity = utl.ALL,
      spacing = 2,
      prefix = utl.prefix,
      -- suffix = ""
      -- format = function() end
    },
  },
}
