return {
  -- Vim globals
  globals = {
    neovide_hide_mouse_when_typing = true,
    neovide_input_macos_option_key_is_meta = "only_left",
    neovide_scale_factor = 1, -- at start
    neovide_default_scale_factor = 1,
  },
  -- Vim options
  options = {
    guifont = "Hack Nerd Font:h14",
    linespace = 0,
  },
  keys = {
    { "<M-=>", require("utils.neovide").scaler(1.25) }, -- increase
    { "<M-->", require("utils.neovide").scaler(1 / 1.25) }, -- decrease
    { "<M-BS>", require("utils.neovide").scaler(-1) }, -- reset
  },
}
