local settings = require("utils.settings")
local lsp = require("settings.lsp")
local menu_keys = require("settings.keys.menu")

-- Set leader keys
vim.g.mapleader = menu_keys.leader
vim.g.maplocalleader = menu_keys.localleader

-- Set vim settings
settings.set(require("settings.vim"))
-- local node_path=vim.fn.expand(vim.g.node_path)
-- local python_path=vim.fn.expand(vim.g.python_path)
-- vim.env.PATH = node_path .. ":" .. vim.env.PATH
-- vim.env.PATH = python_path .. ":" .. vim.env.PATH
-- vim.g.node_host_prog = node_path .. "/neovim-node-host"
-- vim.g.python3_host_prog = python_path .. "/python"

-- Set neovide settings
if vim.g.neovide then
  settings.set(require("settings.neovide"))
end

-- Set LSP settings
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, lsp.hover)
vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, lsp.signature_help)
vim.diagnostic.config(lsp.diagnostics)
