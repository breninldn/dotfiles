-- Vim options
return {
  globals = {
    colorscheme = "tokyonight",
    node_host_prog = vim.fn.expand("~/.nvm/versions/node/v16.20.0/bin/neovim-node-host"),
    python3_host_prog = vim.fn.expand("~/.pyenv/versions/py3nvim/bin/python"),
  },
  options = {
    autowrite = true, -- Enable auto write
    cmdheight = 2,
    clipboard = "unnamedplus", -- Sync with system clipboard
    colorcolumn = "80",
    completeopt = "menu,menuone,noselect",
    conceallevel = 2,
    confirm = true, -- Confirm to save changes before exiting modified buffer
    cursorline = true, -- Enable highlighting of the current line
    expandtab = true, -- Use spaces instead of tabs
    exrc = true,
    foldenable = false,
    formatoptions = "jcroqlnt", -- tcqj
    grepformat = "%f:%l:%c:%m",
    grepprg = "rg , --vimgrep",
    ignorecase = true, -- Ignore case
    inccommand = "nosplit", -- preview incremental substitute
    laststatus = 0,
    list = false, -- Show some invisible characters (tabs...
    listchars = {
      eol = "$",
      extends = ">",
      precedes = "<",
      tab = ">~",
      trail = "!",
      space = ".",
    },
    mouse = "a", -- Enable mouse mode
    number = true, -- Print line number
    pumblend = 10, -- Popup blend
    pumheight = 10, -- Maximum number of entries in a popup
    relativenumber = true, -- Relative line numbers
    scrolloff = 4, -- Lines of context
    sessionoptions = { "buffers", "curdir", "tabpages", "winsize" },
    shiftround = true, -- Round indent
    shiftwidth = 2, -- Size of an indent
    shortmess = {
      l = true,
      t = true,
      T = true,
      o = true,
      F = true,
      O = true,
      W = true,
      I = true,
      c = true,
      C = true,
    },
    showmode = false, -- Dont show mode since we have a statusline
    sidescrolloff = 8, -- Columns of context
    signcolumn = "yes", -- Always show the signcolumn, otherwise it would shift the text each time
    smartcase = true, -- Don't ignore case with capitals
    smartindent = true, -- Insert indents automatically
    splitkeep = "screen",
    spelllang = { "en" },
    splitbelow = true, -- Put new windows below current
    splitright = true, -- Put new windows right of current
    tabstop = 2, -- Number of spaces tabs count for
    termguicolors = true, -- True color support
    timeout = true,
    timeoutlen = 500,
    undofile = true,
    undolevels = 10000,
    updatetime = 200, -- Save swap file and trigger CursorHold
    wildmode = "longest:full,full", -- Command-line completion mode
    winminwidth = 5, -- Minimum window width
    wrap = false, -- Disable line wrap
  },
}
