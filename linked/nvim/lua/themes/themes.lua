return {
  {
    "EdenEast/nightfox.nvim",
    build = ":NightfoxCompile",
    config = function()
      require("themes.configs.nightfox")
    end,
  },
  {
    "catppuccin/nvim",
    name = "catppuccin",
    config = function()
      require("themes.configs.catppuccin")
    end,
  },
  {
    "sainnhe/edge",
    config = function()
      require("themes.configs.edge")
    end,
  },
  {
    "sainnhe/everforest",
    config = function()
      require("themes.configs.everforest")
    end,
  },
  {
    "projekt0n/github-nvim-theme",
    config = function()
      require("themes.configs.github")
    end,
  },
  {
    "marko-cerovac/material.nvim",
    config = function()
      require("themes.configs.material")
    end,
  },
  {
    "savq/melange",
  },
  {
    "tanvirtin/monokai.nvim",
    config = function()
      require("themes.configs.monokai")
    end,
  },
  {
    "rafamadriz/neon",
    config = function()
      require("themes.configs.neon")
    end,
  },
  {
    "navarasu/onedark.nvim",
    config = function()
      require("themes.configs.onedark")
    end,
  },
  {
    "sainnhe/sonokai",
  },
  {
    "folke/tokyonight.nvim",
    opts = require("themes.configs.tokyonight"),
  },
}
