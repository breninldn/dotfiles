-- latte, frappe, macchiato, mocha
vim.g.catppuccin_flavour = "mocha"
require("catppuccin").setup({
  compile_path = vim.fn.stdpath("cache") .. "/catppuccin",
  transparent_background = false,
  term_colors = false,
  dim_inactive = {
    enabled = true,
    shade = "dark",
    percentage = 0,
  },
  styles = {
    comments = { "italic" },
    conditionals = { "italic" },
    loops = {},
    functions = {},
    keywords = {},
    strings = {},
    variables = {},
    numbers = {},
    booleans = {},
    properties = {},
    types = {},
    operators = {},
  },
  integrations = {
    alpha = true,
    cmp = true,
    dap = {
      enabled = true,
      enable_ui = true, -- enable nvim-dap-ui
    },
    gitsigns = true,
    hop = true,
    illuminate = {
      enabled = true,
      lsp = false,
    },
    lsp_trouble = true,
    markdown = true,
    mason = true,
    native_lsp = {
      enabled = true,
      virtual_text = {
        errors = { "italic" },
        hints = { "italic" },
        warnings = { "italic" },
        information = { "italic" },
      },
      navic = true,
      underlines = {
        errors = { "undercurl" },
        hints = { "undercurl" },
        warnings = { "undercurl" },
        information = { "undercurl" },
      },
      inlay_hints = {
        background = true,
      },
    },
    neotest = true,
    -- nvimtree = true,
    overseer = true,
    telescope = true,
    treesitter = true,
    ts_rainbow2 = true,
    which_key = true,

    -- For more plugins integrations please scroll down
    -- (https://github.com/catppuccin/nvim#integrations)
  },
  color_overrides = {},
  custom_highlights = {},
})
-- The following autocmd will change the flavour to latte when you
-- :set background=light and to mocha after :set background=dark

-- vim.api.nvim_create_autocmd("OptionSet", {
--   pattern = "background",
--   callback = function()
--     vim.cmd("Catppuccin " .. (vim.v.option_new == "light" and "latte" or "mocha"))
--   end,
-- })

-- vim.api.nvim_create_autocmd("ColorSchemePre", {
-- 	pattern = "*",
-- 	callback = function()
-- 		print "I ran before loading Catppuccin!"
-- 	end
-- })

-- vim.api.nvim_create_autocmd("ColorScheme", {
-- 	pattern = "*",
-- 	callback = function()
-- 		local colors = require("catppuccin.palettes").get_palette()
-- 		-- do something with colors
-- 	end
-- })
