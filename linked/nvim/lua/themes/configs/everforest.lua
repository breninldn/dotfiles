-- see :help edge for all options

-- For dark version.
vim.cmd "set background=dark"
--For light version.
-- vim.cmd "set background=light"

-- Set contrast.
-- This configuration option should be placed before
-- `colorscheme everforest`.
-- Available values: 'hard', 'medium'(default), 'soft'
vim.g.everforest_background = 'medium'
-- For better performance
vim.g.everforest_better_performance = 1
