require("github-theme").setup({
  -- Change the "hint" color to the "orange" color, and make the "error" color bright red
  -- colors = { hint = "orange", error = "#ff0000" },
  comment_style = "italic",
  function_style = "italic",
  -- Float windows like the lsp diagnostics windows get
  -- a darker background.
  dark_float = true,
  -- Sidebar like windows like NvimTree get a darker background
  dark_sidebar = true,
  -- Highlight style for keywords (check :help highlight-args for options)
  keyword_style = "italic",
  -- Set a darker background on sidebar-like windows.
  sidebars = { "qf", "vista_kind", "terminal", "packer", "treesitter" },
  -- dark, dimmed, dark_default, dark_colorblind, light, light_default, light_colorblind
  -- theme_style = "dark",
  -- Enable this to disable setting the background color
  transparent = false,
  variable_style = "italic",
  -- Overwrite the highlight groups
  -- overrides = function(c)
  --   return {
  --     htmlTag = { fg = c.red, bg = "#282c34", sp = c.hint, style = "underline" },
  --     DiagnosticHint = { link = "LspDiagnosticsDefaultHint" },
  --     -- this will remove the highlight groups
  --     TSField = {},
  --   }
  -- end
})
