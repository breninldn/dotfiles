-- default, atlantis, andromeda, shusia, maia, espresso
vim.g.sonokai_style = 'default'
vim.g.sonokai_better_performance = 1
vim.g.sonokai_enable_italic = 1
vim.g.sonokai_diagnostic_text_highlight = 1
vim.g.sonokai_diagnostic_line_highlight = 1
-- vim.g.sonokai_diagnostic_virtual_text = 'colored'
vim.g.sonokai_current_word = 'grey background'
vim.g.disable_terminal_colors = 1
