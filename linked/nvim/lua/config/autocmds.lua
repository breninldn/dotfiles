-- Autoformat on save
local vim = vim

vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  pattern = { "mrconfig", ".mrconfig" },
  callback = function()
    vim.opt_local.filetype = "dosini"
    vim.opt_local.syntax = "dosini"
    vim.opt_local.commentstring = "# %s"
  end,
})

-- Highlight yanks
vim.api.nvim_create_autocmd("TextYankPost", {
  group = vim.api.nvim_create_augroup("highlight_yank", {}),
  desc = "Hightlight selection on yank",
  pattern = "*",
  callback = function()
    vim.highlight.on_yank({ higroup = "IncSearch", timeout = 200 })
  end,
})

-- Shorten timeout len in insert mode to allow using <esc><esc> effectively
-- in terminal mode
vim.api.nvim_create_autocmd("TermEnter", {
  callback = function()
    vim.cmd("set timeoutlen=200")
  end,
})
vim.api.nvim_create_autocmd("TermLeave", {
  callback = function()
    vim.cmd("set timeoutlen=1000")
  end,
})
