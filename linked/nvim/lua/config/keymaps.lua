local vim = vim
local keys = require("settings.keys.keys")
local menu = require("settings.keys.menu")
local wk = require("which-key")
local term_util = require("utils.terminal")
local file_util = require("utils.files")
local keymaps = require("utils.keymaps")
local test_util = require("utils.test")
local wnd = require("utils.windows")
local map = keymaps.map
local notmap = keymaps.notmap
local imap = keymaps.imap
local nmap = keymaps.nmap
local tmap = keymaps.tmap
local gitsigns = require("gitsigns")
local gitlinker = require("gitlinker")
local gitlinker_actions = require("gitlinker.actions")
local bufremove = require("mini.bufremove")

---- Insert mappings - Mimic Emacs Line Editing in Insert
imap(keys.insert.char.back, "<Left>")
imap(keys.insert.char.forward, "<Right>")
imap(keys.insert.word.back, "<C-Left>")
imap(keys.insert.word.forward, "<C-Right>")
imap(keys.insert.word.delete, "<C-W>") -- gui
imap(keys.insert.line.start, "<Home>")
imap(keys.insert.line._end, "<End>")

---- Terminal mappings
tmap(keys.term.return_to_normal, "<C-\\><C-N>")
tmap(keys.insert.word.delete, "<C-W>") -- gui

---- Normal mappings

-- Buffers
nmap(keys.goto.buffer.next, vim.cmd.bnext)
nmap(keys.goto.buffer.previous, vim.cmd.bprevious)
nmap(keys.goto.buffer.first, vim.cmd.bfirst)
nmap(keys.goto.buffer.last, vim.cmd.blast)
nmap(keys.buffer.write, vim.cmd.w)
nmap(keys.buffer.write_all, vim.cmd.wa)
nmap(keys.buffer.close, bufremove.delete)
nmap(keys.buffer.close_force, function()
  bufremove.delete(0, true)
end)
nmap(keys.buffer.undo_closed, "<cmd>e #<cr>")

-- Close/Quit
local close = keymaps.resolve_menu(menu.close)
wk.add({
  { close.buffer.menu, group = "Close" },
  { close.buffer.current, bufremove.delete, desc = "Close other buffers" },
  { close.buffer.others, "<cmd>BufOnly<cr><c-l>", desc = "Close other buffers" },

  { close.window.menu, group = "Quit" },
  { close.window.all, "<cmd>confirm qa<CR>", desc = "Quit all" },
  { close.window.others, "<cmd>confirm on<CR>", desc = "Quit others" },
})

-- Debug
local debug = keymaps.resolve_menu(menu.debug)
wk.add({
  { debug.menu, group = "Debug" },
  { debug.run_to_cursor, "<cmd>lua require'dap'.run_to_cursor()<cr>", desc = "Run to cursor" },
  { debug.toggle_ui, "<cmd>lua require'dapui'.toggle({reset = true})<cr>", desc = "Toggle UI" },
  { debug.step.back, "<cmd>lua require'dap'.step_back()<cr>", desc = "Step Back" },
  { debug.step.into, "<cmd>lua require'dap'.step_into()<cr>", desc = "Step Into" },
  { debug.step.over, "<cmd>lua require'dap'.step_over()<cr>", desc = "Step Over" },
  { debug.step.out, "<cmd>lua require'dap'.step_out()<cr>", desc = "Step Out" },
  { debug.continue, "<cmd>lua require'dap'.continue()<cr>", desc = "Continue" },
  { debug.session.disconnect, "<cmd>lua require'dap'.disconnect()<cr>", desc = "Disconnect" },
  { debug.session.get, "<cmd>lua require'dap'.session()<cr>", desc = "Get Session" },
  { debug.session.start, "<cmd>lua require'dap'.continue()<cr>", desc = "Start" },
  { debug.session.quit, "<cmd>lua require'dap'.close()<cr>", desc = "Quit" },
  { debug.pause, "<cmd>lua require'dap'.pause()<cr>", desc = "Pause" },
  { debug.repl, "<cmd>lua require'dap'.repl.toggle()<cr>", desc = "Toggle Repl" },
  { debug.toggle_breakpoint, "<cmd>lua require'dap'.toggle_breakpoint()<cr>", desc = "Toggle Breakpoint" },
})

-- Diagnostics
nmap(keys.goto.diagnostics.next, function()
  vim.diagnostic.goto_next({ float = true })
end)
nmap(keys.goto.diagnostics.previous, function()
  vim.diagnostic.goto_prev({ float = true })
end)

-- Edit
nmap(keys.edit.line_break, "i<CR><ESC>")
notmap(keys.edit.clear, "<cmd>nohlsearch|diffupdate|normal! <c-l><cr>")
local edit = keymaps.resolve_menu(menu.edit)

wk.add({
  { edit.menu, group = "Edit" },
  {
    edit.generate.doc.current,
    "<cmd>Neogen<cr>",
    desc = "Generate Documentation" },
  {
    edit.generate.doc.class,
    "<cmd>Neogen class<cr>",
    desc = "Generate Class Documentation",
  },
  {
    edit.generate.doc.type,
    "<cmd>Neogen type<cr>",
    desc = "Generate Type Documentation",
  },
  {
    edit.generate.doc.func,
    "<cmd>Neogen func<cr>",
    desc = "Generate Function Documentation",
  },
  {
    edit.generate.doc.file,
    "<cmd>Neogen file<cr>",
    desc = "Generate File Documentation",
  },
})


-- File
local file = keymaps.resolve_menu(menu.file)
wk.add({
  { file.menu, group = "File" },
  { file.delete, "<cmd>Delete<cr>", desc = "Delete current file" },
  {
    file.delete_force,
    "<cmd>Delete!<cr>",
    desc = "Delete current file (force)",
  },
  {
    file.move,
    "<cmd>exec 'Move ' . input('Move to: ', expand('%:p')) <cr>",
    desc = "Move current file",
  },
  {
    file.rename,
    "<cmd>exec 'Rename ' . input('Rename to: ')<cr>",
    desc = "Rename current file",
  },
})

-- Git
nmap(keys.git.hunk.next, function()
  gitsigns.next_hunk()
  gitsigns.preview_hunk()
end, { desc = "Next Hunk" })
nmap(keys.git.hunk.previous, function()
  gitsigns.prev_hunk()
  gitsigns.preview_hunk()
end, { desc = "Previous Hunk" })

local git = keymaps.resolve_menu(menu.git)
wk.add({
  { git.menu, group = "Git" },
  { git.blame.line, gitsigns.blame_line, desc = "Blame" },
  { git.blame.buffer, "<cmd>Git blame<cr>", desc = "Blame All Lines" },
  {
    git.browse.file,
    function()
      gitlinker.get_buf_range_url("n", {
        action_callback = gitlinker_actions.open_in_browser,
      })
    end,
    desc = "Open git repo file url in browser",
  },
  {
    git.browse.repo,
    function()
      gitlinker.get_repo_url({
        action_callback = gitlinker_actions.open_in_browser,
      })
    end,
    desc = "Open git home url in browser",
  },
  { git.stage.hunk, gitsigns.stage_hunk, desc = "Addd Hunk to Stage" },
  { git.stage.buffer, "<cmd>Gwrite<cr>", desc = "Add Buffer to Stage" },
  { git.stage.undo.hunk, gitsigns.undo_stage_hunk, desc = "Undo Stage Hunk" },
  { git.stage.undo.buffer, gitsigns.reset_buffer_index, desc = "Unstage Buffer File" },
  { git.checkout.menu, group = "Checkout" },
  { git.checkout.branch, "<cmd>Telescope git_branches<cr>", desc = "Branch" },
  { git.checkout.commit.all, "<cmd>Telescope git_commits<cr>", desc = "Commit" },
  { git.checkout.commit.buffer, "<cmd>Telescope git_bcommits<cr>", desc = "Buffer Commit" },
  { git.diff, "<cmd>Gvdiffsplit<cr>", desc = "Git Diff" },
  { git.mergetool, "<cmd>Gvdiffsplit!<cr>", desc = "Merge" },
  { git.edit, "<cmd>Gedit<cr>", desc = "Git edit(fugitive)" },
  { git.hub, "<cmd>Octo<cr>" },
  { git.log.all, "<cmd>Git log<cr>", desc = "Git log" },
  { git.log.buffer, "<cmd>Git log %<cr>", desc = "Git log of current file" },
  { git.push_pull.menu, group = "Push/Pull" },
  { git.push_pull.pull, "<cmd>Git pull<cr>", desc = "Pull" },
  { git.push_pull.pull_force, "<cmd>Git pull --force<cr>", desc = "Force Pull" },
  { git.push_pull.push, "<cmd>Git push<cr>", desc = "Push" },
  { git.push_pull.push_force, "<cmd>Git push --force-with-lease<cr>", desc = "Force Push" },
  { git.reset.hunk, gitsigns.reset_hunk, desc = "Reset Hunk" },
  { git.reset.buffer, "<cmd>Gread<cr>", desc = "Reset Buffer File" },
  { git.status, "<cmd>Git<cr>", desc = "Status" },
  { git.view.hunk, gitsigns.preview_hunk, desc = "View Hunk" },
  { git.checkout.stash, "<cmd>Telescope git_stash<cr>", desc = "Stash" },
  {
    git.copy.file_url,
    function()
      gitlinker.get_buf_range_url("n", { action_callback = gitlinker_actions.copy_to_clipboard })
    end,
    desc = "Copy git repo file url",
  },
  {
    git.copy.repo_url,
    gitlinker.get_repo_url,
    desc = "Copy git repo home url",
  },
})

wk.add({
  mode = { "v" },

  -- Git
  { git.menu, group = "Git (visual)" },
  {
    git.stage.hunk,
    "<cmd>lua require 'gitsigns'.stage_hunk({vim.fn.line('.'), vim.fn.line('v')})<cr>",
    desc = "Stage selected",
  },
  {
    git.browse.file,
    function()
      gitlinker.get_buf_range_url("v", { action_callback = gitlinker_actions.open_in_browser })
    end,
    desc = "Open git repo url in browser (selection)",
  },
  {
    git.copy.file_url,
    function()
      gitlinker.get_buf_range_url("v", { action_callback = gitlinker_actions.copy_to_clipboard })
    end,
    desc = "Copy git repo url (selection)",
  },
  {
    git.reset.hunk,
    "<cmd>lua require 'gitsigns'.reset_hunk({vim.fn.line('.'), vim.fn.line('v')})<cr>",
    desc = "Reset selected",
  },
})

-- Jump in buffer
nmap(keys.goto.char, "<cmd>HopChar1<cr>")
nmap(keys.goto.line, "<cmd>HopLineStart<cr>")

-- LSP
local lsp = keymaps.resolve_menu(menu.lsp)
wk.add({
  { lsp.menu, group = "LSP" },
  { lsp.code_action, vim.lsp.buf.code_action, desc = "Code Action" },
  { lsp.codelens, vim.lsp.codelens.run, desc = "CodeLens Action" },
  {
    lsp.format,
    function()
      require("conform").format({ async = true })
    end,
    desc = "Format",
  },
  {
    lsp.logs,
    "<cmd>lua vim.fn.execute('edit ' .. vim.lsp.get_log_path())<cr>",
    desc = "Open the LSP logfile",
  },
  {
    lsp.signature_help,
    vim.lsp.buf.signature_help,
    desc = "View Signature Help",
  },
  {
    lsp.implementation,
    "<cmd>Telescope lsp_implementations<cr>",
    desc = "Go to Implementation",
  },
  {
    lsp.diagnostics.all,
    "<cmd>Trouble diagnostics toggle<cr>",
    desc = "Diagnostics",
  },
  {
    lsp.diagnostics.buffer,
    "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
    desc = "Buffer diagnostics",
  },
  {
    lsp.diagnostics.hover,
    vim.diagnostic.open_float,
    desc = "View Diagnostics Message",
  },
  {
    lsp.outline,
    "<cmd>Telescope lsp_document_symbols<cr>",
    desc = "Outline"
  },
  {
    lsp.references,
    "<cmd>Telescope lsp_references path_display={'smart'}<cr>",
    desc = "References",
  },
  { lsp.rename, vim.lsp.buf.rename, desc = "Rename Symbol" },
  { lsp.symbols, "<cmd>Telescope lsp_document_symbols<cr>", desc = "Symbols" },
})

-- Notes
local notes = keymaps.resolve_menu(menu.notes)
wk.add({
  { notes.menu, group = "Notes" },
  { notes.new.note, "<cmd>ObsidianNew<cr>", desc = "New note" },
  { notes.new.note_from_template, "<cmd>ObsidianNewFromTemplate<cr>", desc = "New note from template" },
  { notes.new.link, "<cmd>ObsidianLinkNew<cr>", desc = "Insert link to new note" },
  -- { "<leader>ne", "<cmd>PapisEditEntry<cr>", desc = "Edit Papis Entry" },
  -- { "<leader>nE", "<cmd>PapisEditEntry<cr>", desc = "Edit Papis Note" },
  -- { "<leader>nf", "<cmd>PapisOpenFile<cr>", desc = "Papis File" },
  { notes.insert.link, "<cmd>ObsidianLink<cr>", desc = "Insert link to note" },
  { notes.search.backlinks, "<cmd>ObsidianBacklinks<cr>", desc = "Search back links" },
  { notes.search.links, "<cmd>ObsidianLinks<cr>", desc = "Search links" },
  { notes.search.notes, "<cmd>ObsidianSearch<cr>", desc = "Search notes" },
  { notes.search.tags, "<cmd>ObsidianTags<cr>", desc = "Search tags" },
  -- { "<leader>np", "<cmd>Telescope papis<cr>", desc = "Papis" },
  { notes.today, "<cmd>ObsidianToday<cr>", desc = "Today" },
  { notes.tomorrow, "<cmd>ObsidianTomorrow<cr>", desc = "Tomorrow" },
  {
    notes.insert.template,
    "<cmd>ObsidianTemplate<cr>",
    desc = "Insert from template",
  },
  -- { "<leader>nv", "<cmd>PapisShowPopup<cr>", desc = "Papis popup" },
  { notes.workspace, "<cmd>ObsidianWorkspace<cr>", desc = "Switch Workspace" },
  { notes.yesterday, "<cmd>ObsidianYesterday<cr>", desc = "Yesterday" },
})

wk.add({
  mode = { "v" },
  { notes.menu, group = "Notes" },
  { notes.insert.link, "<cmd>ObsidianLink<cr>", desc = "Insert link to note" },
  { notes.new.link, "<cmd>ObsidianLinkNew<cr>", desc = "Insert link to new note" },
  { notes.extract, "<cmd>ObsidianExtractNote<cr>", desc = "Extract new note" },
})

-- Open
nmap(keys.open.files, file_util.find_files)
nmap(keys.open.buffers, "<cmd>Telescope buffers show_all_buffers=true<cr>")
nmap(keys.open.terminal.cwd, term_util.new)
nmap(keys.open.terminal.file_dir, function()
  term_util.new(vim.fn.expand("%:p:h"))
end)
nmap("gx", "<cmd>URLOpenUnderCursor<cr>")

local open = keymaps.resolve_menu(menu.open)
wk.add({
  { open.menu, group = "Open" },
  { open.quickfix, "<cmd>Trouble quickfix toggle<cr>", desc = "Quickfix" },
  { open.location_list, "<cmd>Trouble loclist toggle<cr>", desc = "Location List" },
  { open.recent_files, "<cmd>Telescope oldfiles<cr>", desc = "Recent Files" },
})

-- Search
nmap(keys.search.text, file_util.live_grep)
nmap(keys.search.cursor_word, file_util.grep_string)

local search = keymaps.resolve_menu(menu.search)
wk.add({
  { search.menu, group = "Search" },
  { search.color_scheme, "<cmd>Telescope colorscheme<cr>", desc = "Color scheme" },
  { search.help, "<cmd>Telescope help_tags<cr>", desc = "Help" },
  { search.keys, "<cmd>Telescope keymaps<cr>", desc = "Keymap" },
  -- -- {
  -- --   "<leader>/l",
  -- --   "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",
  -- --   desc = "LSP Symbol",
  -- -- },
  { search.man_page, "<cmd>Telescope man_pages<cr>", desc = "Man Page" },
  { search.todo, "<cmd>TodoTelescop<cr>", desc = "Todo" },
})

-- Tabs
nmap(keys.tab.new, vim.cmd.tabnew)
nmap(keys.tab.next, vim.cmd.tabnext)
nmap(keys.tab.previous, vim.cmd.tabprevious)

-- Tasks
nmap(keys.tasks.run, "<cmd>OverseerRun<cr>")
nmap(keys.tasks.show, "<cmd>OverseerToggle<cr>")

-- Toggles
local toggle = keymaps.resolve_menu(menu.toggle)
wk.add({
  { toggle.menu, group = "Option toggles" },
  { toggle.cursor.column, "<cmd>set cuc! cuc?<cr>", desc = "Toggle Cursor Column" },
  { toggle.cursor.line, "<cmd>set cul! cul?<cr>", desc = "Toggle Cursor Line" },
  { toggle.fold, "<cmd>set fen! fen?<cr>", desc = "Toggle Fold Enable" },
  { toggle.list_chars, "<cmd>set list! list?<cr>", desc = "Toggle List Characters" },
  { toggle.numbers, "<cmd>set nu! nu?<cr>", desc = "Toggle Numbers" },
  { toggle.numbers_relative, "<cmd>set rnu! rnu?<cr>", desc = "Toggle Relative Numbers" },
  { toggle.spell, "<cmd>set spell! spell?<cr>", desc = "Toggle Spell" },
  { toggle.color_codes, "<cmd>ColorizerToggle<cr>", desc = "Toggle Color Codes" },
})

-- Windows
map(keys.window.quit, "<cmd>confirm close<cr>")
map(keys.window.quit_all, "<cmd>qa!<cr>")

map(keys.window.nav.left, wnd.go_left)
map(keys.window.nav.right, wnd.go_right)
map(keys.window.nav.up, wnd.go_up)
map(keys.window.nav.down, wnd.go_down)

map(keys.window.split.horizontal, vim.cmd.split)
map(keys.window.split.vertical, vim.cmd.vsplit)

map(keys.window.move.left, wnd.take_left)
map(keys.window.move.right, wnd.take_right)
map(keys.window.move.up, wnd.take_up)
map(keys.window.move.down, wnd.take_down)
map(keys.window.fullscreen, "<cmd>ZenMode<cr>")
map(keys.window.push.left, wnd.push_left)
map(keys.window.push.right, wnd.push_right)
map(keys.window.push.up, wnd.push_up)
map(keys.window.push.down, wnd.push_down)

map(keys.window.resize.left, wnd.resize_left)
map(keys.window.resize.right, wnd.resize_right)
map(keys.window.resize.up, wnd.resize_up)
map(keys.window.resize.down, wnd.resize_down)
map(keys.window.resize.equal, "<C-W>=")

-- Projects
nmap(keys.open.projects, "<cmd>Telescope workspaces<cr>")
local project = keymaps.resolve_menu(menu.project)
wk.add({
  { project.menu, group = "Project" },
  { project.add, "<cmd>WorkspacesAdd<cr>", desc = "Add cwd to projects" },
  { project.remove, "<cmd>WorkspacesRemove<cr>", desc = "Remove cwd from projects" },
})

-- Run
local run = keymaps.resolve_menu(menu.run)
wk.add({
  { run.menu, group = "Run" },
  {run.test.nearest, test_util.run_test , desc = "Run Nearest Test" },
  {run.test.buffer, test_util.run_buffer_tests, desc = "Run Buffer Tests" },
  {run.test.summary, test_util.toggle_summary, desc = "Toggle Test Summary" },
  -- { "<leader>ra", <function 1>, desc = "Attach To Running Test" },
  -- { "<leader>rd", <function 1>, desc = "Debug Nearest Test" },
  { run.command, "<cmd>Telescope commands<cr>", desc = "Run command" },
  -- { "<leader>rx", <function 1>, desc = "Stop Running Test" },
})

-- Status
local status = keymaps.resolve_menu(menu.status)
wk.add({
  { status.menu, group = "Status" },
  { status.plugins, "<cmd>Lazy show<cr>", desc = "Plugins" },
  { status.treesitter, "<cmd>TSConfigInfo<cr>", desc = "Treesitter Info" },
  { status.mason, "<cmd>Mason<cr>", desc = "Mason Info" },
  { status.conform, "<cmd>ConformInfo<cr>", desc = "Conform Info" },
  { status.lsp, "<cmd>LspInfo<cr>", desc = "Lsp status" },
})

-- wk.add({
--   -- -- Kubernetes
--   -- { "<leader>k", group = "Kubernetes" },
--   -- { "<leader>kC", "<cmd>SelectSplitCRD<CR>", desc = "Download CRD Split" },
-- -- { "<leader>kD", "<cmd>DeleteNamespace<CR>", desc = "Kubectl Delete Namespace" },
--   -- { "<leader>kK", "<cmd>OpenK9sSplit<CR>", desc = "Split View K9s" },
--   -- { "<leader>kT", "<cmd>HelmDryRun<CR>", desc = "Helm DryRun Buffer" },
--   -- { "<leader>ka", "<cmd>KubectlApplyFromBuffer<CR>", desc = "Kubectl Apply From Buffer" },
--   -- {
--   --   "<leader>kb",
--   --   "<cmd>HelmDependencyBuildFromBuffer<CR>",
--   --   desc = "Helm Dependency Build",
--   -- },
--   -- { "<leader>kc", "<cmd>SelectCRD<CR>", desc = "Download CRD" },
--   -- {
--   --   "<leader>kd",
--   --   "<cmd>HelmDeployFromBuffer<CR>",
--   --   desc = "Helm Deploy Buffer to Context",
--   -- },
--   -- { "<leader>kf", "<cmd>JsonFormatLogs<CR>", desc = "Format JSON" },
--   -- { "<leader>kk", "<cmd>OpenK9s<CR>", desc = "Open K9s" },
--   -- { "<leader>kl", "<cmd>ToggleYamlHelm<CR>", desc = "Toggle YAML/Helm" },
--   -- {
--   --   "<leader>kr",
--   --   "<cmd>RemoveDeployment<CR>",
--   --   desc = "Helm Remove Deployment From Buffer",
--   -- },
--   -- { "<leader>kt", "<cmd>HelmTemplateFromBuffer<CR>", desc = "Helm Template From Buffer" },
--   -- {
--   --   "<leader>ku",
--   --   "<cmd>HelmDependencyUpdateFromBuffer<CR>",
--   --   desc = "Helm Dependency Update",
--   -- },
--   -- { "<leader>kv", "<cmd>ViewPodLogs<CR>", desc = "View Pod Logs" },
-- })
