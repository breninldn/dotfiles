local vim = vim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  -- bootstrap lazy.nvim
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  spec = {
    { import = "themes" },
    { import = "plugins" },
    { import = "plugins.ui" },
    { import = "plugins.editing" },
    { import = "plugins.files" },
    { import = "plugins.autocomplete" },
    { import = "plugins.project" },
    { import = "plugins.lang.syntax" },
    { import = "plugins.lang.lsp" },
    { import = "plugins.lang.test" },
    { import = "plugins.lang" },
  },
  defaults = {
    lazy = true,
    version = "*",
  },
  -- colorscheme that will be used when installing plugins.
  install = { colorscheme = { "tokyonight" } },
  -- automatically check for plugin updates
  checker = {
    enabled = false,
    concurrency = 2,
    frequency = 24 * 60 * 60 * 7, -- every week
  },
  -- automatically check for config file changes
  change_detection = {
    enabled = false,
    notify = false,
  },
  ui = {
    -- a number <1 is a percentage., >1 is a fixed size
    size = { width = 0.8, height = 0.8 },
    wrap = true, -- wrap the lines in the ui
    -- The border to use for the UI window.
    -- Accepts same border values as |nvim_open_win()|.
    border = require("settings.ui").border,
    title = "Plugins",
    title_pos = "center",
  },
})
