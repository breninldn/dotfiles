#!/usr/bin/env bash
set -euo pipefail

source "$SHELOB_HOME/logger.sh" "og"
source "$SHELOB_HOME/filesystem.sh"
source "$(source_dir)/lib/og/og_gitconfig"
source "$(source_dir)/lib/og/og_repo_files"
source "$(source_dir)/lib/og/og_update_org_config"

REPO="$PWD"
org_dir="${REPO%/*}"
repo="${REPO##*/}"
org="${org_dir##*/}"
org_repo="$org/$repo" # org/repo
org_config="$(chezmoi source-path)/projects/$org"

_error() {
	error "$org_repo: $1"
	exit 1
}

_info() {
	info "$org_repo: $1"
}

_run() {
	debug "$*"
	"$@"
}

[[ "$REPO" =~ $HOME/projects/ ]] || _error "Not in any orgs!"
org_config="$(realpath "$org_config")" || _error "No .org dir found"

cmd="${1:-}"
if [[ "$cmd" == "gitconfig" ]]; then
	_run _git_config "$org_config/org/gitconfig"
elif [[ "$cmd" == "repo_files" ]]; then
	_run _link_repo_files "$org_config/org/repo/$repo"
elif [[ "$cmd" == "update_org_config" ]]; then
	_run _update_org_config "$org_config" "$repo"
else
	_error "Invalid command $cmd"
fi

# vim: ft=sh
