local wezterm = require("wezterm")
local action = wezterm.action
local sv = require("skipvim").skip
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- Config
config.front_end = "WebGpu"

config.color_scheme = "Catppuccin Mocha"

-- Tab bar
config.enable_tab_bar = true
config.use_fancy_tab_bar = false
config.hide_tab_bar_if_only_one_tab = true
config.tab_bar_at_bottom = true

-- Opacity
config.window_background_opacity = 0.85
config.window_decorations = "TITLE|RESIZE"

-- Fonts
config.font = wezterm.font("Hack Nerd Font Mono", { weight = "Medium" })
config.font_size = 13
config.warn_about_missing_glyphs = false

-- Keys
config.leader = { key = "Space", mods = "CTRL|SHIFT", timeout_millisecond = 1000 }
config.keys = {
	{
		key = "c",
		mods = "LEADER",
		action = action.ActivateCopyMode,
	},
	{
		key = "Backspace",
		mods = "CTRL",
		action = action.SendKey({ key = "w", mods = "CTRL" }),
	},
	{
		key = "Enter",
		mods = "ALT",
		action = action.DisableDefaultAssignment,
	},
	{
		key = "-",
		mods = "CTRL",
		action = action.DisableDefaultAssignment,
	},
	{
		key = "=",
		mods = "CTRL",
		action = action.DisableDefaultAssignment,
	},
	{
		key = "=",
		mods = "ALT",
		action = action.IncreaseFontSize,
	},
	{
		key = "Backspace",
		mods = "ALT",
		action = action.ResetFontSize,
	},
	{
		key = "-",
		mods = "ALT",
		action = action.DecreaseFontSize,
	},
	{
		key = "'",
		mods = "CTRL",
		action = action.SendKey({ key = "'", mods = "CTRL" }),
	},
	sv({
		key = "n",
		mods = "ALT",
		action = action.SpawnTab("CurrentPaneDomain"),
	}),
	sv({
		key = "x",
		mods = "ALT",
		action = action.CloseCurrentTab({ confirm = true }),
	}),
	sv({
		key = "X",
		mods = "ALT",
		action = action.CloseCurrentTab({ confirm = false }),
	}),
	sv({
		key = ";",
		mods = "ALT",
		action = action.SplitVertical({ domain = "CurrentPaneDomain" }),
	}),
	sv({
		key = "'",
		mods = "ALT",
		action = action.SplitHorizontal({ domain = "CurrentPaneDomain" }),
	}),
	sv({
		key = "q",
		mods = "ALT",
		action = action.CloseCurrentPane({ confirm = true }),
	}),
	-- Navigate tabs
	sv({ key = "]", mods = "ALT", action = action.ActivateTabRelative(1) }),
	sv({ key = "[", mods = "ALT", action = action.ActivateTabRelative(-1) }),
	-- Move tabs
	sv({ key = "]", mods = "CTRL|ALT", action = action.MoveTabRelative(1) }),
	sv({ key = "[", mods = "CTRL|ALT", action = action.MoveTabRelative(-1) }),
	-- move between split panes
	sv({ key = "h", mods = "ALT", action = action.ActivatePaneDirection("Left") }),
	sv({ key = "l", mods = "ALT", action = action.ActivatePaneDirection("Right") }),
	sv({ key = "k", mods = "ALT", action = action.ActivatePaneDirection("Up") }),
	sv({ key = "j", mods = "ALT", action = action.ActivatePaneDirection("Down") }),
	-- -- resize panes
	sv({ key = "h", mods = "ALT|SHIFT", action = action.AdjustPaneSize({ "Left", 3 }) }),
	sv({ key = "l", mods = "ALT|SHIFT", action = action.AdjustPaneSize({ "Right", 3 }) }),
	sv({ key = "k", mods = "ALT|SHIFT", action = action.AdjustPaneSize({ "Up", 3 }) }),
	sv({ key = "j", mods = "ALT|SHIFT", action = action.AdjustPaneSize({ "Down", 3 }) }),
}

-- Scrollback
config.scrollback_lines = 3500

-- Make neovim zen mode integrate with wezterm
wezterm.on("user-var-changed", function(window, pane, name, value)
	local overrides = window:get_config_overrides() or {}
	if name == "ZEN_MODE" then
		local incremental = value:find("+")
		local number_value = tonumber(value)
		if incremental ~= nil then
			while number_value > 0 do
				window:perform_action(action.IncreaseFontSize, pane)
				number_value = number_value - 1
			end
			overrides.enable_tab_bar = false
		elseif number_value < 0 then
			window:perform_action(action.ResetFontSize, pane)
			overrides.font_size = nil
			overrides.enable_tab_bar = true
		else
			overrides.font_size = number_value
			overrides.enable_tab_bar = false
		end
	end
	window:set_config_overrides(overrides)
end)

-- Navigate between vim windows and wezterm panes

return config
