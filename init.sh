#!/usr/bin/env bash
set -euo pipefail

# Install chezmoi and clone dotfiles
chezmoi init --recurse-submodules=false https://gitlab.com/breneser/dotfiles.git

source_path="$(chezmoi source-path)"

source <(chezmoi execute-template <"$source_path"/dot_env.tmpl)

# Apply
chezmoi apply
